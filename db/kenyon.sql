-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2021 at 01:40 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kenyon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-In-active',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) UNSIGNED NOT NULL,
  `course_name` varchar(100) NOT NULL,
  `course_duration` varchar(100) NOT NULL,
  `course_outcomes` longblob NOT NULL,
  `course_desc` longblob NOT NULL,
  `thumbnail` text NOT NULL,
  `path` text NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '0-Un-publish, 1-Publish',
  `deleted` int(2) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `course_enroll`
--

CREATE TABLE `course_enroll` (
  `id` int(11) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` longtext NOT NULL,
  `description` longblob NOT NULL,
  `status` enum('0') NOT NULL DEFAULT '0' COMMENT '0-Client-user',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(11) UNSIGNED NOT NULL,
  `forum_name` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` longblob NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `tot_likes` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-In-active',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL,
  `created_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `forum_reply`
--

CREATE TABLE `forum_reply` (
  `id` int(11) UNSIGNED NOT NULL,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `tot_likes` int(11) NOT NULL,
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1- Deleted',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `forum_like`
--

CREATE TABLE `forum_like` (
  `id` int(11) UNSIGNED NOT NULL,
  `forum_reply_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-admin ,1-user',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `phone_no` varchar(55) DEFAULT NULL,
  `address` text,
  `footer` varchar(55) DEFAULT NULL,
  `facebook_url` varchar(55) DEFAULT NULL,
  `linkedin_url` varchar(55) DEFAULT NULL,
  `youtube_url` varchar(55) DEFAULT NULL,
  `about` text,
  `term_condition` text,
  `cancellation_policy` text NOT NULL,
  `return_policy` text NOT NULL,
  `privacy_policy` text NOT NULL,
  `logo` varchar(55) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `custom_id` varchar(10) NOT NULL,
  `subject` longblob NOT NULL,
  `description` longblob NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Open, 1-Closed',
  `created_on` datetime NOT NULL,
  `close_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_reply`
--

CREATE TABLE `ticket_reply` (
  `id` int(11) UNSIGNED NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `msg` longblob NOT NULL,
  `flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-Admin, 1-User',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - Staff-admin, 1 - Team-admin, 2 - Client-user',
  `country` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `is_admin` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - Normal-user, 1 - Admin-user',
  `status` enum('0','1','2','3') NOT NULL DEFAULT '2' COMMENT '0 - Active, 1 - In-active, 2 - Email-not-verified, 3 - Deleted',
  `verified_on` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_purchase`
--

CREATE TABLE `user_purchase` (
  `id` int(11) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `transc_no` varchar(100) NOT NULL,
  `transc_date` datetime NOT NULL,
  `transc_amt` float NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Success,1-Failed',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_type` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - Staff-admin, 1 - Team-admin, 2 - Client-user',
  `code` varchar(100) NOT NULL,
  `coupon_amt` float NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Un-publish, 1-Publish',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_enroll`
--
ALTER TABLE `course_enroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_reply`
--
ALTER TABLE `forum_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_like`
--
ALTER TABLE `forum_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_reply`
--
ALTER TABLE `ticket_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_purchase`
--
ALTER TABLE `user_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_enroll`
--
ALTER TABLE `course_enroll`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forum_reply`
--
ALTER TABLE `forum_reply`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forum_like`
--
ALTER TABLE `forum_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_reply`
--
ALTER TABLE `ticket_reply`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_purchase`
--
ALTER TABLE `user_purchase`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `user` CHANGE `country` `country` VARCHAR(255) NOT NULL;

ALTER TABLE `voucher` CHANGE `start_date` `start_date` DATETIME NOT NULL;
ALTER TABLE `voucher` CHANGE `end_date` `end_date` DATETIME NOT NULL;

ALTER TABLE `settings`  ADD `cancellation_policy` TEXT NOT NULL  AFTER `term_condition`,  ADD `return_policy` TEXT NOT NULL  AFTER `cancellation_policy`,  ADD `privacy_policy` TEXT NOT NULL  AFTER `return_policy`;

ALTER TABLE `voucher`  ADD `usage_per_user` INT(11) NOT NULL DEFAULT '0'  AFTER `end_date`;

ALTER TABLE `forum_reply`  ADD `flag` INT(2) NOT NULL DEFAULT '0' COMMENT '0-Admin, 1-User'  AFTER `user_id`;

ALTER TABLE `course` ADD `discount` FLOAT NOT NULL AFTER `price`;

ALTER TABLE `course` CHANGE `status` `status` INT(2) NOT NULL DEFAULT '0' COMMENT '0-Un-publish, 1-Publish';

ALTER TABLE `course` CHANGE `deleted` `deleted` INT(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1- Deleted';

ALTER TABLE `user` ADD `moodle_id` INT(11) NULL AFTER `id`;

ALTER TABLE `user` ADD `first_name` VARCHAR(55) NOT NULL AFTER `moodle_id`, ADD `last_name` VARCHAR(55) NOT NULL AFTER `first_name`;

ALTER TABLE `user` CHANGE `user_type` `role_id` INT(11) NOT NULL DEFAULT '4';

ALTER TABLE `user` ADD `created_by` INT(11) NOT NULL AFTER `created_on`;

ALTER TABLE `user` ADD `deleted` INT(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1- Deleted' AFTER `updated_on`;