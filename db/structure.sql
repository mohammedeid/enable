--
-- Database: `kenyon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-In-active',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `course_name` varchar(100) NOT NULL,
  `course_duration` varchar(100) NOT NULL,
  `course_outcomes` longblob NOT NULL,
  `course_desc` longblob NOT NULL,
  `thumbnail` text NOT NULL,
  `path` text NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '0-Un-publish, 1-Publish',
  `deleted` int(2) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `course_enroll`
--

CREATE TABLE `course_enroll` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` longtext NOT NULL,
  `description` longblob NOT NULL,
  `status` enum('0') NOT NULL DEFAULT '0' COMMENT '0-Client-user',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `forum_name` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` longblob NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `tot_likes` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-In-active',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL,
  `created_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `forum_reply`
--

CREATE TABLE `forum_reply` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `tot_likes` int(11) NOT NULL,
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1- Deleted',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `forum_like`
--

CREATE TABLE `forum_like` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `forum_reply_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-admin ,1-user',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------
--
-- Table structure for table `site_settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `site_name` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `phone_no` varchar(55) DEFAULT NULL,
  `address` text,
  `footer` varchar(55) DEFAULT NULL,
  `facebook_url` varchar(55) DEFAULT NULL,
  `linkedin_url` varchar(55) DEFAULT NULL,
  `youtube_url` varchar(55) DEFAULT NULL,
  `about` text,
  `term_condition` text,
  `cancellation_policy` text NOT NULL,
  `return_policy` text NOT NULL,
  `privacy_policy` text NOT NULL,
  `logo` varchar(55) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `custom_id` varchar(10) NOT NULL,
  `subject` longblob NOT NULL,
  `description` longblob NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Open, 1-Closed',
  `created_on` datetime NOT NULL,
  `close_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `ticket_reply`
--

CREATE TABLE `ticket_reply` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `msg` longblob NOT NULL,
  `flag` int(2) NOT NULL DEFAULT '0' COMMENT '0-Admin, 1-User',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - Staff-admin, 1 - Team-admin, 2 - Client-user',
  `country` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `is_admin` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 - Normal-user, 1 - Admin-user',
  `status` enum('0','1','2','3') NOT NULL DEFAULT '2' COMMENT '0 - Active, 1 - In-active, 2 - Email-not-verified, 3 - Deleted',
  `verified_on` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `user_purchase`
--

CREATE TABLE `user_purchase` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `transc_no` varchar(100) NOT NULL,
  `transc_date` datetime NOT NULL,
  `transc_amt` float NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Success,1-Failed',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_type` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - Staff-admin, 1 - Team-admin, 2 - Client-user',
  `code` varchar(100) NOT NULL,
  `coupon_amt` float NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0-Un-publish, 1-Publish',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '0-Active, 1-Deleted',
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;






 
