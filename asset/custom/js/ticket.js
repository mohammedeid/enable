$(function () {
    $('#ticket').DataTable({
       "responsive": true,
       "autoWidth": false,
       initComplete: function () {
       }
    });

    $('#user_ticket').DataTable({
        "responsive": true,
        "autoWidth": false,
        initComplete: function () {
        }
    });

    $('#addEditModal').on('show.bs.modal', function (e) {
        var loadurl = e.relatedTarget.dataset.loadUrl;
        $(this).find('.modal-content').load(loadurl, function() {
            initModal();
        });
    });
    
    $('#addEditModal').on('hide.bs.modal', function (e) {
       $(this).find('.modal-content').html('');
    });
});

function initModal(){
	tinymce.init({
		selector:'textarea[name="description"]',
		menubar: false,
	});
}


	
