$(function () {

   $('#settingModal').on('show.bs.modal', function (e) {
      var loadurl = e.relatedTarget.dataset.loadUrl;
      $(this).find('.modal-content').load(loadurl, function() {
         initModal();
      });
   });
   
   $('#settingModal').on('hide.bs.modal', function (e) {
      $(this).find('.modal-content').html('');
       tinyMCE.editors=[];
   });

});

function initModal(){
	tinymce.init({
		selector:'textarea[name="about"],textarea[name="term_condition"],textarea[name="privacy_policy"],textarea[name="return_policy"],textarea[name="cancellation_policy"]',
		menubar: false,
	});
}