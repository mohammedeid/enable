$(function () {
    $('#faq').DataTable({
       "responsive": true,
       "autoWidth": false,
       initComplete: function () {
       }
    });
 
    $('#addEditModal').on('show.bs.modal', function (e) {
        var loadurl = e.relatedTarget.dataset.loadUrl;
        $(this).find('.modal-content').load(loadurl, function() {
            initModal();
        });
    });
    
    $('#addEditModal').on('hide.bs.modal', function (e) {
       $(this).find('.modal-content').html('');
    });

    $("#deleteModal").on('show.bs.modal', function(e){
         var loadValue = e.relatedTarget.dataset.loadValue;
         $(this).find('.modal-content #faq_id').val(loadValue);
     });
 });

 function initModal(){
	tinymce.init({
		selector:'textarea[name="desc"]',
		menubar: false,
	});
}