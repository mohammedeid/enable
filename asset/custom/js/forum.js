$(function () {
    $('#forum').DataTable({
       "responsive": true,
       "autoWidth": false,
       initComplete: function () {
       }
    });
 
    $('#addEditModal').on('show.bs.modal', function (e) {
        var loadurl = e.relatedTarget.dataset.loadUrl;
        $(this).find('.modal-content').load(loadurl); 
    });
    
    $('#addEditModal').on('hide.bs.modal', function (e) {
       $(this).find('.modal-content').html('');
    });

    $("#deleteModal").on('show.bs.modal', function(e){
         var loadValue = e.relatedTarget.dataset.loadValue;
         $(this).find('.modal-content #forum_id').val(loadValue);
     });
 });