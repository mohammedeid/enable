$(function () {
   $('#user').DataTable({
      "responsive": true,
      "autoWidth": false,
      initComplete: function () {
      }
   });

   $('#addEditModal').on('show.bs.modal', function (e) {
      var loadurl = e.relatedTarget.dataset.loadUrl;
      $(this).find('.modal-content').load(loadurl);
   });
   
   $('#addEditModal').on('hide.bs.modal', function (e) {
      $(this).find('.modal-content').html('');
   });

   $("#deleteModal").on('show.bs.modal', function(e){
		var loadValue = e.relatedTarget.dataset.loadValue;
		$(this).find('.modal-content #user_id').val(loadValue);
	});
});

function showHideAdmin(Obj){
   $("#team_admin").val('');
   $("#staff_admin").val('');
   if($(Obj).val()==4){
      $("#admin-holder").show();
      $("#team_admin").prop('required',true);
      $("#staff_admin").prop('required',true);
   } else {
      $("#admin-holder").hide();
      $("#team_admin").prop('required',false);
      $("#staff_admin").prop('required',false);
   }
  
}