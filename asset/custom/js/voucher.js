$(function () {
    $('#voucher').DataTable({
       "responsive": true,
       "autoWidth": false,
       initComplete: function () {
       }
    });
 
    $('#addEditModal').on('show.bs.modal', function (e) {
        var loadurl = e.relatedTarget.dataset.loadUrl;
        $(this).find('.modal-content').load(loadurl, function(){
            initAddEditModal();
        }); 
    });
    
    $('#addEditModal').on('hide.bs.modal', function (e) {
       $(this).find('.modal-content').html('');
    });

    $("#deleteModal").on('show.bs.modal', function(e){
         var loadValue = e.relatedTarget.dataset.loadValue;
         $(this).find('.modal-content #voucher_id').val(loadValue);
     });
 });

 function initAddEditModal(){
	$('.dtpicker').appendDtpicker({
		"minuteInterval": 15, 
		"calendarMouseScroll": false,
		"futureOnly": true, 
		"autodateOnStart": false,
		onInit: function(handler) {
			var picker = handler.getPicker();
			$(picker).addClass('main-datetimepicker');
		}
	});

	$('#startdt').change(function() {
	    $('#enddt').handleDtpicker('setMinDate', $('#startdt').val()); //set end datetime not lower then start datetime
	});
}