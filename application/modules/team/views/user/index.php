<?=modules::run('common/header');?>
<div class="container-fluid">
    <!-- Page Breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Page Content here -->
            <div class="card card-body">
                <div class="table-responsive">
                <table class="table table-bordered table-hover" id="user">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">#</th>
                            <th>Name</th>
                            <th>User Name</th>
                            <th>Email ID</th>
                            <th>Mobile</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($users){
                            $i=0;
                            foreach($users as $user) {
                                # code...
                        ?>
                                <tr>
                                    <td class="text-center" scope='row'><?=++$i;?></td>
                                    <td><?=$user->first_name." ".$user->last_name;?></td>
                                    <td><?=$user->name;?></td>
                                    <td><?=$user->email_id;?></td>
                                    <td><?=$user->mobile_no;?></td>
                                </tr>
                        <?php
                            }
                            }
                        ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>