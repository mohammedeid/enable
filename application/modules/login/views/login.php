<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>

      <meta charset="UTF-8">
      <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="Description" content="">
      <meta name="Author" content="">
      <meta name="Keywords" content=""/>

      <!-- Title -->
      <title>kenyon</title>

      <!-- Favicon -->
      <link rel="icon" href="<?=base_url();?>asset/img/brand/favicon.png" type="image/x-icon"/>

      <!-- Icons css -->
      <link href="<?=base_url();?>asset/css/icons.css" rel="stylesheet">

      <!--  Right-sidemenu css -->
      <link href="<?=base_url();?>asset/plugins/sidebar/sidebar.css" rel="stylesheet">

      <!--  Custom Scroll bar-->
      <link href="<?=base_url();?>asset/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>

      <!--  Left-Sidebar css -->
      <link rel="stylesheet" href="<?=base_url();?>asset/css/sidemenu.css">

      <!--- Style css --->
      <link href="<?=base_url();?>asset/css/style.css" rel="stylesheet">

      <!--- Dark-mode css --->
      <link href="<?=base_url();?>asset/css/style-dark.css" rel="stylesheet">

      <!---Skinmodes css-->
      <link href="<?=base_url();?>asset/css/skin-modes.css" rel="stylesheet" />

      <!--- Animations css-->
      <link href="<?=base_url();?>asset/css/animate.css" rel="stylesheet">
  </head>
  <body class="main-body bg-light">

    <!-- Loader -->
    <div id="global-loader">
      <img src="<?=base_url();?>asset/img/loader.svg" class="loader-img" alt="Loader">
    </div>
    <!-- /Loader -->
    
    <!-- Page -->
    <div class="page" style="background: url(<?=base_url();?>asset/img/login-bg.jpg); background-size: cover; background-position: top center;">
    
      <div class="container-fluid">
        <div class="row no-gutter" style="margin-top: 8.5vw;">
          <!-- The image half -->
          <div class="col-md-8 col-lg-8 col-xl-8 d-none d-md-flex">
            <div class="row wd-100p mx-auto text-center">
              <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
                <!-- <img src="<?=base_url();?>asset/img/media/login.png" class="my-auto ht-xl-80p wd-md-100p wd-xl-80p mx-auto" alt="logo"> -->
              </div>
            </div>
          </div>
          <!-- The content half -->
          <div class="col-md-3 col-lg-3 col-xl-3 bg-white">
            <div class="align-items-center py-4">
              <!-- Demo content-->
              <div class="container p-0">
                <div class="row">
                  <div class="col-md-10 col-lg-10 col-xl-10 mx-auto">
                    <div class="card-sigin">
                      <div class="d-flex mb-4"> 
                        <a href="<?=base_url();?>" class="text-center">
                          <img src="<?=base_url();?>asset/img/logo.png" class="img-fluid" style="width: 70%;">
                        </a>
                      </div>
                      <div class="card-sigin">
                        <div class="main-signup-header">
                          <?php if($msg){?>
                            <div id="msg" class="alert alert-danger"><?=$msg;?></div>
                          <?php }?>
                          <h5 class="font-weight-semibold mb-3 text-center" style="color: #656565; font-weight: 400 !important; font-size: 16px;">Please sign in to continue.</h5>
                          <?=form_open($this->config->item('moodle_url').'login/index_sso.php', 'name="form"');?>
                            <input type="hidden" name="project_url" value="<?=base_url();?>">
                            <input type="hidden" name="login_url" value="login">
                            <input type="hidden" name="succ_url" value="login/login_sso/login_succ">
                            <div class="form-group">
                              <label class="mb-0">User name</label> <input name="username" class="form-control" placeholder="Enter user name" type="text" required="required">
                            </div>
                            <div class="form-group">
                              <label class="mb-0">Password</label> <input name="password" class="form-control" placeholder="Enter password" type="password" required="required">
                            </div>
                            <button type="submit" class="btn btn-main-primary btn-block">Log In</button>
                          <?=form_close();?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- End -->
            </div>
          </div><!-- End -->
        </div>
      </div>
      
    </div>
    <!-- End Page -->

  <!-- JQuery min js -->

  <script src="<?=base_url();?>asset/plugins/jquery/jquery.min.js"></script>

  <!-- Bootstrap Bundle js -->
  <script src="<?=base_url();?>asset/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Ionicons js -->
  <script src="<?=base_url();?>asset/plugins/ionicons/ionicons.js"></script>

  <!-- Moment js -->
  <script src="<?=base_url();?>asset/plugins/moment/moment.js"></script>

  <!-- eva-icons js -->
  <script src="<?=base_url();?>asset/js/eva-icons.min.js"></script>

  <!-- Rating js-->
  <script src="<?=base_url();?>asset/plugins/rating/jquery.rating-stars.js"></script>
  <script src="<?=base_url();?>asset/plugins/rating/jquery.barrating.js"></script>

  <!-- custom js -->
  <script src="<?=base_url();?>asset/js/custom.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#msg").fadeOut(3000);
    });
  </script>
  </body>
</html>