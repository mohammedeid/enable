<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {
    private $data;
    function __construct(){
        parent::__construct();
        $this->load->model('login_model');
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;  
    }

    function index(){
        $this->isLoggedIn();
    }

    function error(){
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE){
            $this->load->view('login');
        }else {
            redirect('pageNotFound');
        }
    }

    function noaccess() {
        $this->global['pageTitle'] = 'Access Denied';
        $this->datas();

        $this->load->view ( 'includes/header', $this->global );
		$this->load->view ( 'access' );
		$this->load->view ( 'includes/footer' );
    }

    function isLoggedIn() {
        $isLoggedIn = $this->session->userdata('kenyon_lms_userdata') ? $this->session->userdata('kenyon_lms_userdata')['isLoggedIn'] : false;
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            if (isset($_GET['msg'])) {
                # code...
                $data['msg'] = $_GET['msg'];
            }else {
                $data['msg'] = '';
            }
            $this->load->view('login', $data);
        }else {
            redirect('/dashboard');
        }
    }
    
    // function loginMe() {
    //     $this->form_validation->set_rules('username', 'User Name', 'required|max_length[128]|trim');
    //     $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
    //     if($this->form_validation->run() == FALSE) {
    //         $this->index();
    //     }else {
    //         $username = $this->security->xss_clean($this->input->post('username'));
    //         $password = $this->input->post('password');
            
    //         $result = $this->login_model->loginMe($username, $password);
    //         if($result) {
    //             $sessionArray = array('user_id'=>$result->id,         
    //                                   'user_name'=>$result->name,
    //                                   'profile_image' => 'asset/img/faces/6.jpg',
    //                                   'isLoggedIn' => TRUE
    //                                 );

    //             $this->session->set_userdata('kenyon_lms_userdata',$sessionArray);
    //             unset($sessionArray['user_id'], $sessionArray['isLoggedIn']);

    //             redirect('/dashboard');
                
    //         }else {
    //             $this->session->set_flashdata('error', 'Email address not verified');
    //             redirect('/login');
    //         }
    //     }
    // }

    function logout(){
        session_destroy();
		$this->session->unset_userdata('kenyon_lms_userdata');
		
		redirect('/login');
	}

    
}