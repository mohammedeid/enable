<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_sso extends MX_Controller {
    private $data;
    function __construct(){
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('login_model');
    }

    function index(){
    	echo 'hello';
    }

    function login_succ(){
    	$this->load->helper('cookie');
        $cookie = unserialize($this->input->cookie('userLoginInfo',TRUE));
        $result = $this->login_model->custom_login($cookie['id']);
        if ($result) {
            # code...
            $sessionArray = array(  
                                    'user_id'=>$result->id,                    
                                    'moodle_id'=>$result->moodle_id,
                                    'role_id'=>$result->role_id,
                                    'role_text'=>$result->role_name,
                                    'user_name'=>$result->name,
                                    'first_name'=>$result->first_name,
                                    'last_name'=>$result->last_name,
                                    'profile_image' => isset($result->image_name) ? "asset/upload/".$result->image_name : 'asset/img/faces/6.jpg',
                                    'status'=> $result->status,
                                    'isLoggedIn' => TRUE,
                                    'sesskey'=>$cookie['session']
                                );
            $this->session->set_userdata('kenyon_lms_userdata',$sessionArray);

            if($result->role_id==1) {
                redirect('/admin');
            } elseif ($result->role_id==2) {
                redirect('/staff');
            } elseif ($result->role_id==3) {
                redirect('/team');
            } else {
                redirect('/user');
            }
        }else {
            redirect('?msg=invalid credentials');
        }
    }
}