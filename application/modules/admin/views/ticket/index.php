<?=modules::run('common/header');?>
<div class="container-fluid">
    <!-- Page Breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
         <!-- Page Content here -->
         <div class="card card-body">
            <div class="table-responsive">
               <table class="table table-bordered table-hover" id="ticket">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">#</th>
                            <th>Ticket ID</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($tickets){
                                $i=0;
                                foreach($tickets as $value) {
                                    # code...
                                    $ticketStatus=get_ticket_status($value->status);
                                    if($value->status==0){
                                        $status = "<span class='text-danger'>".$ticketStatus."</span>";
                                        $action = anchor('admin/ticket/change_status/1/'.$value->id,'Mark as completed', 'data-popup="tooltip" title="Click to close ticket" class="text-status"');
                                    } else {
                                        $status = "<span class='text-success'>".$ticketStatus."</span>";
                                        $action = anchor('admin/ticket/change_status/0/'.$value->id,'Reopen Ticket', 'data-popup="tooltip" title="Click to reopen ticket" class="text-status"');
                                    }
                                    $ticket_no = "<div><a href='".base_url()."admin/ticket/viewTicketResponse/".$value->id."' >".$value->custom_id."</a></div><div class='datetext-holder'>Created : (".date('d-M-y',strtotime($value->created_on)).")</div>";
                                ?>
                                <tr>
                                    <td class="text-center" scope='row'><?=++$i;?></td>
                                    <td><?=$ticket_no;?></td>
                                    <td><?=$value->name;?></td>
                                    <td><?=$value->subject;?></td>
                                    <td><?=$status;?></td>
                                    <td class="text-center"><?=$action;?></td>
                                </tr>
                                <?php
                                }
                            }
                        ?>
                    </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<?=modules::run('common/footer');?>