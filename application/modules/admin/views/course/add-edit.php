<?=form_open_multipart('admin/course/add');?>
<input type="hidden" name="id" id="id" value="<?=($content)?$content->id:'';?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?=$content ? "Edit" :"Add";?> Course</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="course">Select Course <span class="text-danger">*</span></label>
                <select name="course" class="form-control" required="required">
                <option value="">Select Course</option>
                 <?php 
                    foreach ($scormcourses as $key => $scormcourse) {
                        $selected = "";
                        if($content->m_course_name):
                            if($scormcourse->name == $content->m_course_name):
                                $selected = "selected";
                            endif;
                        endif;    
                    ?>
                     <option value="<?=$scormcourse->id;?>" <?=$selected;?> ><?=$scormcourse->name;?></option> 
                
                <?php } ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="course_name">Course Title <span class="text-danger">*</span></label>
                <input type="text" name="course_name" class="form-control" value="<?=$content ? $content->course_name : '';?>" required placeholder="Enter course title">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="price">Price <span class="text-danger">*</span></label>
                <input type="number" name="price" class="form-control" value="<?=$content ? $content->price : '';?>" required placeholder="Enter course price">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="discount">Discount<small>(%)</small> </label>
                <input type="number" name="discount" class="form-control" value="<?=$content ? $content->discount : '';?>"  placeholder="Enter course discount %">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="course_duration">Course Duration (In Mins)<span class="text-danger">*</span></label>
                <input type="text" name="course_duration" class="form-control" value="<?=$content ? $content->course_duration : '';?>" required placeholder="Enter course duration">
            </div>
        </div>                
        <div class="col">
            <div class="form-group">
                <label for="thumbnail">Thumbnail<span class="text-danger">*</span></label>
                <input type="file" name="thumbnail" class="form-control-file" />
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="course_desc">Description<span class="text-danger">*</span></label>
                <textarea name="course_desc" class="form-control" rows="6" ><?=$content ? $content->course_desc : '';?></textarea>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="course_outcomes">Course Outcomes<span class="text-danger">*</span></label>
                <textarea name="course_outcomes" class="form-control" rows="6" ><?=$content ? $content->course_outcomes : '';?></textarea>
            </div>
        </div>
    </div>
   
    
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>

