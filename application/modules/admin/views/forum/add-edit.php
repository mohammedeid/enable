<?=form_open_multipart('admin/forum/add');?>
<input type="hidden" name="id" id="id" value="<?=($content)?$content->id:'';?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?=$content ? "Edit" :"Add";?> Forum</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="forum_name">Forum Name <span class="text-danger">*</span></label>
                <input type="text" name="forum_name" class="form-control" id="forum_name" value="<?=$content ? $content->forum_name : '';?>" required placeholder="Enter forum name">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="title">Title <span class="text-danger">*</span></label>
                <input type="text" name="title" class="form-control" id="title" value="<?=$content ? $content->title : '';?>" required placeholder="Enter title">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" class="form-control" rows="10" id="description"><?=$content ? $content->description : '';?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="upload_image">Upload Image</label>
                <input type="file" name="upload_image" class="form-control-file" id="upload_image" />
            </div>
        </div>
    </div>
    
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>

