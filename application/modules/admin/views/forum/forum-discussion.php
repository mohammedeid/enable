<?=modules::run('common/header');?>
<div class="container-fluid">
    <!-- Page Breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <!-- Page Content here -->
            <div class="card card-body">
                <ul class="media-list">
                    <li class="media">
                        <div class="media-left">
                            <div class="thumb">
                                <?php
                                if($content->image_name){
                                    $imgUrl=base_url()."asset/img/forum/".$content->image_name;
                                } else {
                                    $imgUrl=base_url()."asset/img/no-img.png";
                                }
                                ?>
                                <a href="#">
                                    <img src="<?=$imgUrl;?>" class="img-responsive img-rounded media-preview" alt="Demo">
                                </a>
                            </div>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading"><a href="#"><?=$content->title;?></a></h6>
                            <ul class="list-inline list-inline-separate text-muted mb-3">
                                <li><i class="icon-user position-left"></i> <?=$details->name;?></li>
                                <li><i class="fas fa-clock"></i> <?= date('d M, Y',strtotime($content->created_on));?></li>
                            </ul>
                            <?=$content->description;?><hr>
                            <span class="pull-right">
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#post"><i class="fa fa-reply"></i> Post Reply</button>
                            </span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $responses; ?> 
        </div>
    </div>
</div>

<div class="modal effect-scale" id="post" data-backdrop="static">
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
         <?=form_open('admin/forum/add_response');?>
            <input type="hidden" name="forum_id" id="forum_id" value="<?=$forum_id;?>">
            <div class="modal-header">
               <h5 class="modal-title">Post Comment</h5>
               <button type="button" class="close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <textarea name="message" class="form-control" rows="5" id="message" placeholder="Enter post here..."></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">POST</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
         <?=form_close();?>
		</div>
	</div>
</div>
<?=modules::run('common/footer');?>