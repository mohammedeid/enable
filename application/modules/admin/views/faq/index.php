<?=modules::run('common/header');?>
<div class="container-fluid">
  <!-- Page Breadcrumb -->
  <div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
      <div class="d-flex">
        <h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
      </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
      <div class="pr-1 mb-3 mb-xl-0">
        <a href="#" data-load-url="<?=base_url();?>admin/faq/view" data-toggle="modal" data-target="#addEditModal" class="btn btn-icon btn-primary btn-sm mr-2">
            <i class="fe fe-plus"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="faq">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if($faqs){
                  $i=0;
                  foreach($faqs as $faq) {
              ?>
                    <tr>
                      <td><?=++$i;?></td>
                      <td><?=$faq->title;?></td>
                      <td class="text-center">
                          <a href="#" data-load-url="<?=base_url();?>admin/faq/view/<?=$faq->id;?>" data-toggle="modal" title="Edit" data-target="#addEditModal"><i class="fe fe-edit"></i></a>
                          <a href="#" data-load-value="<?=$faq->id;?>" data-toggle="modal" title="Delete" data-target="#deleteModal"><i class="fe fe-trash-2 text-danger"></i></a>
                      </td>
                    </tr>
              <?php
                  }
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal effect-scale" id="addEditModal" data-backdrop="static">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content"></div>
   </div>
</div>

<div class="modal effect-scale" id="deleteModal" data-backdrop="static">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content">
         <?=form_open('admin/faq/delete');?>
            <input type="hidden" name="faq_id" id="faq_id" value="">
            <div class="modal-header">
               <h5 class="modal-title">Delete User FAQ</h5>
               <button type="button" class="close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="form-row">
                  <div class="col">
                     <div class="form-group">
                        <h4><i class="icon-warning2 text-orange-400"></i> Are you sure you want to delete?</h4>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">YES</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
            </div>
         <?=form_close();?>
		</div>
	</div>
</div>

<?=modules::run('common/footer');?>
