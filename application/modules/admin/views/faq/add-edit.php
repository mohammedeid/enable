<?=form_open('admin/faq/add');?>
<input type="hidden" name="id" id="id" value="<?=$content ? $content->id : '';?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?=$content ? "Update" : 'Add';?> FAQ</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
   <div class="form-row">
      <div class="col">
         <div class="form-group">
            <label class="control-label">Title <span class="text-danger">*</span></label>
            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?=$content ? $content->title : '';?>" required>
         </div>
      </div>
   </div>
   <div class="form-row">
      <div class="col">
         <div class="form-group">
            <label class="control-label">Description <span class="text-danger">*</span></label>
            <textarea name="desc" id="editor1" class="form-control" rows="4" required><?=$content ? $content->description : '';?></textarea>
         </div>
      </div>
   </div>
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>