<?=modules::run('common/header');?>
<div class="container-fluid">
	<!-- Page Breadcrumb -->
	<div class="breadcrumb-header justify-content-between">
		<div class="my-auto">
			<div class="d-flex">
				<h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
			</div>
		</div>
	</div>
</div>
<?=modules::run('common/footer');?>
