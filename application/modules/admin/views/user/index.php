<?=modules::run('common/header');?>
<div class="container-fluid">
   <!-- Page Breadcrumb -->
   <div class="breadcrumb-header justify-content-between">
      <div class="my-auto">
         <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
         </div>
      </div>
      <div class="d-flex my-xl-auto right-content">
         <div class="pr-1 mb-3 mb-xl-0">
            <a href="#" data-load-url="<?=base_url();?>admin/user/view" data-toggle="modal" data-target="#addEditModal" class="btn btn-icon btn-primary btn-sm mr-2">
               <i class="fe fe-plus"></i>
            </a>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-md-12">
         <!-- Page Content here -->
         <div class="card card-body">
            <div class="table-responsive">
               <table class="table table-bordered table-hover" id="user">
                  <thead class="thead-light">
                     <tr>
                        <th class="text-center">#</th>
                        <th>Name</th>
                        <th>User Name</th>
                        <th>Email ID</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th class="text-center">Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        if($users){
                           $i=0;
                           foreach($users as $user) {
                              # code...
                     ?>
                              <tr>
                                 <td class="text-center" scope='row'><?=++$i;?></td>
                                 <td><?=$user->first_name." ".$user->last_name;?></td>
                                 <td><?=$user->name;?></td>
                                 <td><?=$user->email_id;?></td>
                                 <td><?=$user->mobile_no;?></td>
                                 <td><?=$user->role;?></td>
                                 <td class="text-center">
                                    <a href="#" data-load-url="<?=base_url();?>admin/user/view/<?=$user->id;?>" data-toggle="modal" title="Edit" data-target="#addEditModal"><i class="fe fe-edit"></i></a>
                                    <a href="#" data-load-value="<?=$user->id;?>" data-toggle="modal" title="Delete" data-target="#deleteModal"><i class="fe fe-trash-2 text-danger"></i></a>
                                 </td>
                              </tr>
                     <?php
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal effect-scale" id="addEditModal" data-backdrop="static">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content"></div>
   </div>
</div>

<div class="modal effect-scale" id="deleteModal" data-backdrop="static">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content">
         <?=form_open('admin/user/delete');?>
            <input type="hidden" name="user_id" id="user_id" value="">
            <div class="modal-header">
               <h5 class="modal-title">Delete User</h5>
               <button type="button" class="close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="form-row">
                  <div class="col">
                     <div class="form-group">
                        <h4><i class="icon-warning2 text-orange-400"></i> Are you sure you want to delete?</h4>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">YES</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
            </div>
         <?=form_close();?>
		</div>
	</div>
</div>
<?=modules::run('common/footer');?>