<?=form_open('admin/user/add');?>
<input type="hidden" name="id" id="id" value="<?=($user)?$user->id:'';?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?=$user ? "Edit" :"Add";?> User</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
   <div class="form-row">
      <div class="col">
         <div class="form-group">
            <label for="firstName">First Name <span class="text-danger">*</span></label>
            <input type="text" name="first_name" class="form-control" id="firstName" value="<?=$user ? $user->first_name : '';?>" required placeholder="Enter first name">
         </div>
      </div>
      <div class="col">
         <div class="form-group">
            <label for="lastName">Last Name <span class="text-danger">*</span></label>
            <input type="text" name="last_name" class="form-control" id="lastName" value="<?=$user ? $user->last_name : '';?>" required placeholder="Enter last name">
         </div>
      </div>
   </div>
   <div class="form-row">
      <div class="col">
         <div class="form-group">
            <label for="email">Email ID <span class="text-danger">*</span></label>
            <input type="email" name="email" name="email" class="form-control" id="email" value="<?=$user ? $user->email_id : '';?>" <?=$user ? "readonly" : '';?> placeholder="Enter email ID" required>
         </div>
      </div>
      <div class="col">
         <div class="form-group">
            <label for="mobileNo">Mobile No <span class="text-danger">*</span></label>
            <input type="number" name="mobile_no" class="form-control" id="mobileNo" value="<?=$user ? $user->mobile_no : '';?>" placeholder="Enter mobile no" required>
         </div>
      </div>
   </div>
   <div class="form-row">
      <div class="col">
         <div class="form-group">
            <label for="country">Country<span class="text-danger">*</span></label>
            <?=form_dropdown('country', $countries, $user ? $user->country : '', 'class="form-control" required');?>
         </div>
      </div>
      <div class="col">
         <div class="form-group">
            <label for="role">User Role <span class="text-danger">*</span></label>
            <?=form_dropdown('role', $roles, $user ? $user->role_id : '', 'class="form-control" required onChange="showHideAdmin(this);"'  );?>
         </div>
      </div>
   </div>
   <div id="admin-holder">
      <div class="form-row" >
         <div class="col">
            <div class="form-group">
               <label for="staffAdmin">Staff Admin<span class="text-danger">*</span></label>
               <?=form_dropdown('staff_admin', $staff, $user ? $user->staff_id : '', 'class="form-control" id="staff_admin"');?>
            </div>
         </div>
         <div class="col" >
            <div class="form-group">
               <label for="role">Team Admin<span class="text-danger">*</span></label>
               <?=form_dropdown('team_admin', $team, $user ? $user->team_id : '', 'class="form-control" id="team_admin"');?>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>

<script>
   <?php if($user && $user->role_id==4): ?>
      $("#admin-holder").show();
      $("#team_admin").prop('required',true);
      $("#staff_admin").prop('required',true);
   <?php else: ?>
      $("#admin-holder").hide();
      $("#team_admin").prop('required',false);
      $("#staff_admin").prop('required',false);
      $("#team_admin").val('');
      $("#staff_admin").val('');
   <?php endif; ?>
</script>