<?=form_open('admin/voucher/add');?>
<input type="hidden" name="id" id="id" value="<?=($content)?$content->id:'';?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?=$content ? "Edit" :"Add";?> Voucher</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="code">Code <span class="text-danger">*</span></label>
                <input type="text" name="code" class="form-control" id="code" value="<?=$content ? $content->code : '';?>" required placeholder="Enter coupon code">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="amount">Amount <span class="text-danger">*</span></label>
                <input type="number" name="amount" class="form-control" id="amount" value="<?=$content ? $content->coupon_amt : '';?>" required placeholder="Enter coupon amount">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="start_date" >Start Date<span class="text-danger">*</span></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                        </div>
                    </div>
                    <input name="start_date" class="form-control dtpicker" id="startdt" type="text" value="<?=($content) ? date('Y-m-d H:m', strtotime($content->start_date)): date('Y-m-d H:m');?>">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="end_date" >End Date<span class="text-danger">*</span></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                        </div>
                    </div>
                    <input name="end_date" class="form-control dtpicker" id="enddt" type="text" value="<?=($content) ? date('Y-m-d H:m', strtotime($content->end_date)): date('Y-m-d H:m', strtotime("+1 day"));?>">
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="role">User Role <span class="text-danger">*</span></label>
                <?=form_dropdown('role', $roles, $content ? $content->role_id : '', 'class="form-control" required');?>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="usage">No. of Usage Per User <span class="text-danger">*</span></label>
                <input type="number" name="usage" class="form-control" id="usage" value="<?=$content ? $content->usage_per_user : '';?>" required placeholder="Enter Usage Per User">
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>

