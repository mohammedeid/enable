<?=modules::run('common/header');?>
<div class="container-fluid">
   <!-- Page Breadcrumb -->
   <div class="breadcrumb-header justify-content-between">
      <div class="my-auto">
         <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
         </div>
      </div>
      <div class="d-flex my-xl-auto right-content">
         <div class="pr-1 mb-3 mb-xl-0">
            <a href="#" data-load-url="<?=base_url();?>admin/voucher/view" data-toggle="modal" data-target="#addEditModal" class="btn btn-icon btn-primary btn-sm mr-2">
               <i class="fe fe-plus"></i>
            </a>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-md-12">
         <!-- Page Content here -->
         <div class="card card-body">
            <div class="table-responsive">
               <table class="table table-bordered table-hover" id="voucher">
                  <thead class="thead-light">
                     <tr>
                        <th style="width:4%;" class="text-center">#</th>
                        <th style="width:11%;">Code</th>
                        <th style="width:10%;">Amount</th>
                        <th style="width:12%;">Role</th>
                        <th style="width:16%;">Start Date</th>
                        <th style="width:16%;">End Date</th>
                        <th>Usage Per User</th>
                        <th style="width:14%;">Status</th>
                        <th class="text-center">Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        if($vouchers){
                              $i=0;
                              foreach($vouchers as $voucher) {
                              # code...
                                 $statusTitle=get_status($voucher->status);
                                 if($voucher->status==0){
                                    $status = "<span class='text-danger'>".$statusTitle."</span>&nbsp;
                                                <a href='".base_url()."admin/voucher/change_status/1/".$voucher->id."' class='text-status'><i class='fas fa-sync-alt'></i></a>";
                                 } else {
                                    $status = "<span class='text-success'>".$statusTitle."</span>&nbsp;
                                                <a href='".base_url()."admin/voucher/change_status/0/".$voucher->id."' class='text-status'><i class='fas fa-sync-alt'></i></a>";
                                 }
                     ?>
                                 <tr>
                                       <td class="text-center" scope='row'><?=++$i;?></td>
                                       <td><?=$voucher->code;?></td>
                                       <td><?=number_format($voucher->coupon_amt);?></td>
                                       <td><?=$voucher->role;?></td>
                                       <td><?=date('d-M-y H:i A',strtotime($voucher->start_date));?></td>
                                       <td><?=date('d-M-y H:i A',strtotime($voucher->end_date));?></td>
                                       <td><?=$voucher->usage_per_user;?></td>
                                       <td><?= $status;?></td>
                                       <td class="text-center">
                                          <a href="#" data-load-url="<?=base_url();?>admin/voucher/view/<?=$voucher->id;?>" data-toggle="modal" title="Edit" data-target="#addEditModal"><i class="fe fe-edit"></i></a>
                                          <a href="#" data-load-value="<?=$voucher->id;?>" data-toggle="modal" title="Delete" data-target="#deleteModal"><i class="fe fe-trash-2 text-danger"></i></a>
                                       </td>
                                 </tr>
                    <?php
                              }
                        }
                    ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal effect-scale" id="addEditModal" data-backdrop="static">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content"></div>
   </div>
</div>

<div class="modal effect-scale" id="deleteModal" data-backdrop="static">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content">
         <?=form_open('admin/voucher/delete');?>
            <input type="hidden" name="voucher_id" id="voucher_id" value="">
            <div class="modal-header">
               <h5 class="modal-title">Delete Voucher</h5>
               <button type="button" class="close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="form-row">
                  <div class="col">
                     <div class="form-group">
                        <h4><i class="icon-warning2 text-orange-400"></i> Are you sure you want to delete?</h4>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">YES</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
            </div>
         <?=form_close();?>
		</div>
	</div>
</div>
<?=modules::run('common/footer');?>