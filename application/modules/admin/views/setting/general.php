<?=form_open_multipart('admin/setting/insert');?>
<div class="modal-header">
  <h5 class="modal-title">Genral Settings</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-6">
      <div class="form-group">
        <label for="siteName">Site Name</label>
        <input type="text" name="site_name" class="form-control" id="siteName" placeholder="Site name" value="<?=$setting->site_name;?>">
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="<?=$setting->email;?>">
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="phone_no">Phone no.</label>
        <input type="text" name="phone_no" class="form-control" id="phone_no" placeholder="Phone no." value="<?=$setting->phone_no;?>">
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="fb">Facebook URL</label>
        <input type="text" name="facebook_url" class="form-control" id="fb" placeholder="Facebook URL" value="<?=$setting->facebook_url;?>">
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="linkdin">Linkedin URL</label>
        <input type="text" name="linkdin" class="form-control" id="linkdin" placeholder="Linkedin URL" value="<?=$setting->linkedin_url;?>">
      </div>
    </div> 
    <div class="col-6">
      <div class="form-group">
        <label for="youtube">Youtube URL</label>
        <input type="text" name="youtube" class="form-control" id="youtube" placeholder="Youtube URL" value="<?=$setting->youtube_url;?>">
      </div>
    </div>
    <div class="col-6">
    <div class="form-group">
      <label for="address">Address</label>
      <textarea name="address" class="form-control"><?=$setting->address;?></textarea>
    </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="footer">Footer</label>
        <textarea name="footer" class="form-control"><?=$setting->footer;?></textarea>
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="exampleFormControlFile1">Logo</label>
        <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label for="exampleFormControlFile1">Logo Preview</label><br>
        <img src="<?=base_url('uploads/'.$setting->logo);?>" alt="logo" class="img-thumbnail" width="100px">
      </div>
    </div>
    
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>
