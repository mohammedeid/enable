<?=form_open('admin/setting/update_terms');?>
<div class="modal-header">
  <h5 class="modal-title">Term and Condition</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <textarea name="term_condition" class="form-control"><?=$setting->term_condition;?></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>
