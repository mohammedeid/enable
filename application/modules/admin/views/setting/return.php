<?=form_open('admin/setting/update_return');?>
<div class="modal-header">
  <h5 class="modal-title">Return Policy</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <textarea name="return_policy" class="form-control"><?=$setting->return_policy;?></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>
