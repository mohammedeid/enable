<?=form_open('admin/setting/update_privacy');?>
<div class="modal-header">
  <h5 class="modal-title">Privacy Policy</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <textarea name="privacy_policy" class="form-control"><?=$setting->privacy_policy;?></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>