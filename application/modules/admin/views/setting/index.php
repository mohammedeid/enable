<?=modules::run('common/header');?>
<div class="container-fluid">
   <!-- Page Breadcrumb -->
   <div class="breadcrumb-header justify-content-between">
      <div class="my-auto">
         <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">Site Settings</h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"></span>
         </div>
      </div>
   </div>

   <div class="row">
      <!-- Page Content here -->
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/general');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-cogs text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">General settings</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/about');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-headset text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">About</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/termcondition');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-exclamation-triangle text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">Terms and Conditons</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/cancellation');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-ban text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">Cancellation Policy</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/return');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-undo-alt text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">Return Policy</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="#" data-load-url="<?=base_url('admin/setting/view/privacy');?>" data-toggle="modal" data-target="#settingModal">
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-user-secret text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">Privacy Policy</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-xl-3 col-md-6 col-lg-6 col-sm-6">
         <a href="<?=base_url('admin/faq');?>"  >
            <div class="card">
               <div class="card-body">
                  <div class="plan-card text-center">
                     <i class="fas fa-question-circle text-primary plan-icon"></i>
                     <h6 class="text-drak text-uppercase mt-4">User FAQ's</h6>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>

</div>
<?=modules::run('common/footer');?>

<div class="modal" tabindex="-1" role="dialog" id="settingModal">
  <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
   </div>
    
  </div>
</div>