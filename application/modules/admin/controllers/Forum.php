<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'forum';
		$this->load->model('user_m');
        $this->load->model('forum_m');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = 'Manage Forums';
		$this->data['forums'] = $this->forum_m->get_forum();
		
		$this->load->view('forum/index',$this->data);
	}

    public function view($id = null){
		$this->data['content'] = array();
	
		if($id){
			$this->data['content'] = $this->forum_m->get_forum_detail($id);
		}
		
		$this->load->view('forum/add-edit',$this->data);
	}

    function add(){
        $this->form_validation->set_rules('forum_name', 'Forum Name', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
       
        if ($this->form_validation->run() == TRUE){
        	$id = $this->input->post('id');
        	$dataArr['forum_name'] = $this->input->post('forum_name');
            $dataArr['title'] = $this->input->post('title');
            $dataArr['description'] = $this->input->post('description');
            if(isset($_FILES['upload_image']['tmp_name'])){
				$config['upload_path']          = './asset/img/forum/';
                $config['allowed_types']        = 'jpeg|jpg|png';
                // $config['max_size']             = 100;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('upload_image')){
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $uploadData = $this->upload->data();
                    $dataArr['image_name'] =  $uploadData['file_name'];
                }
		    }
            
        	if($id){
				$this->forum_m->update($id, $dataArr);
        		redirect_success('admin/forum',"Updated dataArr");
        	}else{
				$dataArr['created_by'] = $this->user['user_id'];
                $dataArr['created_on'] = date('Y-m-d H:i:s');
        		$this->forum_m->insert($dataArr);
        		redirect_success('admin/forum',"Added Successfully");
        	}
        }

        redirect_error('admin/forum',"failed");
	}

	function delete(){
		$id = $this->input->post('forum_id');
		$this->forum_m->update($id, array("deleted"=>1));
		
        redirect_success('admin/forum',"Updated Successfully");
	}

    function change_status($status, $forumID){
		$this->forum_m->update($forumID, array('status'=>$status));
		
        redirect_success('admin/forum',"Updated Successfully");	
	}

	function forum_discussion($forumID){
		$user_id=$this->user['user_id'];
		$this->data['forum_id']=$forumID;
		$this->data['content'] =$content= $this->forum_m->get_forum_detail($forumID);
		$this->data['details'] =$this->user_m->get($content->created_by);
		$responses=$this->forum_m->get_forum_response($forumID);
		$rowStr="";
		if($responses): 
			$rowStr="<ul class='custom-timeline' style='padding-bottom: 0px; max-width: none;'>"; 
			foreach ($responses as $key => $value) {
				if($value->image_name){
					$imageUrl=base_url()."asset/upload/".$value->image_name;
				} else {
					$imgUrl=base_url()."asset/upload/user-icon.png";
				}
				if($this->forum_m->check_response_like($value->id,$user_id)){
					$likeUrl="<a href='".base_url()."admin/forum/like_response/1/".$forumID."/".$value->id."' class='text-status'>Liked <i class='far fa-thumbs-up text-blue-700 forum-like'></i></a>";
				} else {
					$likeUrl="<a href='".base_url()."admin/forum/like_response/0/".$forumID."/".$value->id."' class='text-status'><span class='text-muted'>Like</span> <i class='far fa-thumbs-up text-blue-700 forum-like'></i></a>";
				}
				$rowStr.="<li class='custom-timeline-item custom-timeline-item-detailed' >"
						."  <div class='custom-timeline-content'>"
						."      <div class='custom-timeline-avatar'>"
						."          <img src='".$imgUrl."' alt='Avatar' class='circle'>"
						."      </div>"
						."      <div class='custom-timeline-header'>"
						."          <span class='custom-timeline-autor'>".$value->reply_name."</span><br>"
						."          <p class='custom-timeline-activity'>".$value->message."</p>"
						."      </div>"
						."      <hr style='margin-bottom: 2px;'>"
						."      <div class='text-right text-muted'>"
						."          <span>".$value->tot_likes."</span> "
						."           ".$likeUrl." | ".date('d M, Y H:i A',strtotime($value->reply_date))." "
						."          &nbsp;<a href='".base_url()."admin/forum/delete_response/".$forumID."/".$value->id."' data-toggle='tooltip' data-original-title='Click to Delete'><i class='fas fa-trash-alt text-danger'></i></a>"							
						."      </div>"
						."  </div>"
						."</li>";
			}
			$rowStr.="</ul>";
		endif;
		$this->data['responses']=$rowStr;

		$this->load->view('forum/forum-discussion',$this->data);
	}

	function add_response(){
		$userID = $this->user['user_id'];
		$forumID=$this->input->post('forum_id');
		$response=$this->input->post('message', TRUE);
		if ($response) {
			$det = array(
						'forum_id'=>$forumID,
						'user_id'=>$userID,
						'message'=>$response,
						'created_on'=>current_date()
					);
			$this->forum_m->saveForumResponse($det);	
		}	

		redirect("admin/forum/forum_discussion/".$forumID);	
	}

	function delete_response($forumID,$replyID){
		$this->forum_m->deleteForumResponse($replyID);

		redirect("admin/forum/forum_discussion/".$forumID);	
	}

	function like_response($status,$forumID,$replyID){
		$userID = $this->user['user_id'];
		if($status==1):
			$this->forum_m->deleteResponseLike($replyID,$userID);
		else:
			$data = array(
				'forum_reply_id'=>$replyID,
				'user_id'=>$userID,
				'created_on'=>current_date()
			);
			$this->forum_m->addResponseLike($replyID,$data);
		endif;		

		redirect("admin/forum/forum_discussion/".$forumID);	
	}

}
