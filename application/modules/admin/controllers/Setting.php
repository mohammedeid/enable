<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MX_Controller {

	private $data;
	public function __construct(){
		parent::__construct();
	    $this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'setting';
	    $this->load->model('settings_m');
	}

	public function index(){
		$this->load->view('setting/index', $this->data);
	}

	public function view($type){
		$this->data['setting'] = $this->settings_m->get(1);
		if($type == 'general'){
			$this->load->view('setting/general', $this->data);
		}else if($type == 'about'){
			$this->load->view('setting/about', $this->data);
		}else if($type == 'termcondition'){
			$this->load->view('setting/termcondition', $this->data);
		}else if($type == 'cancellation'){
			$this->load->view('setting/cancellation', $this->data);
		}else if($type == 'return'){
			$this->load->view('setting/return', $this->data);
		}else if($type == 'privacy'){
			$this->load->view('setting/privacy', $this->data);
		}
	}

	public function insert(){
		$site_name = $this->input->post('site_name');
		$email= $this->input->post('email');
		$phone_no = $this->input->post('phone_no');
		$address= $this->input->post('address');
		$footer = $this->input->post('footer');
		$facebook_url = $this->input->post('facebook_url');
		$linkedin_url = $this->input->post('linkdin');
		$youtube_url = $this->input->post('youtube');
		$updated_at = date('YYYY-mm-dd HH:mm:s');

		$det = array(
			'site_name'=>$site_name,
			'email'=>$email,
			'phone_no'=>$phone_no,
			'address'=>$address,
			'footer'=>$footer,
			'facebook_url'=>$facebook_url,
			'linkedin_url'=>$linkedin_url,
			'youtube_url'=>$youtube_url,
			'updated_at'=>$updated_at,
		);
		
		if(isset($_FILES['logo']['tmp_name'])){
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png';
			// $config['max_size']             = 100;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('logo')){
				$error = array('error' => $this->upload->display_errors());
			} else {
				$data = array('upload_data' => $this->upload->data());
				$uploadData = $this->upload->data();
				$det['logo'] =  $uploadData['file_name'];
			}
		}
		
		$this->settings_m->updateSettings('1',$det);

		redirect_success('admin/setting','updated successfully');

	}

    function update_terms(){
		$term_condition = $this->input->post('term_condition');
		$this->settings_m->update('1', array('term_condition'=>$term_condition));
		
        redirect_success('admin/setting','updated successfully');	
	}

	function update_about(){
		$about = $this->input->post('about');
		$this->settings_m->update('1', array('about'=>$about));
		
        redirect_success('admin/setting','updated successfully');	
	}

	function update_cancellation(){
		$cancellation_policy = $this->input->post('cancellation_policy');
		$this->settings_m->update('1', array('cancellation_policy'=>$cancellation_policy));
		
        redirect_success('admin/setting','updated successfully');	
	}

	function update_return(){
		$return_policy = $this->input->post('return_policy');
		$this->settings_m->update('1', array('return_policy'=>$return_policy));
		
        redirect_success('admin/setting','updated successfully');	
	}

	function update_privacy(){
		$privacy_policy = $this->input->post('privacy_policy');
		$this->settings_m->update('1', array('privacy_policy'=>$privacy_policy));
		
        redirect_success('admin/setting','updated successfully');	
	}



}
?>