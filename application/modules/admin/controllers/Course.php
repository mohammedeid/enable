<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'course';
        $this->load->model('course_m');
		$this->load->model('moodle');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = 'Manage Courses';
		$this->data['courses'] = $this->course_m->get_course();
		$this->load->view('course/index',$this->data);
	}

	public function view($id = null){
		$this->data['content'] = array();
		$this->data['scormcourses'] = $this->moodle->get_all_scorm_courses();
		if($id){
			$this->data['content'] = $this->course_m->get_course_detail($id);
		}
		
		$this->load->view('course/add-edit',$this->data);
	}  

	function add(){
        $this->form_validation->set_rules('course_name', 'Course Name', 'required');
        $this->form_validation->set_rules('course_duration', 'Course Duration', 'required');
		$this->form_validation->set_rules('course_desc', 'Course Description', 'required');
        $this->form_validation->set_rules('course_outcomes', 'Course Outcomes', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');

        if ($this->form_validation->run() == TRUE) {
        	$id = $this->input->post('id');

			$dataArr['scorm_id'] = $scorm_id = $this->input->post('course');
			$course = $this->moodle->get_scorm_by_course_id($scorm_id);
			$dataArr['m_course_name'] = $course->name;
			$scoe=$this->moodle->get_scorm_scoes($scorm_id);
			$dataArr['scoe_id'] =$scoe->id;

        	$dataArr['course_name'] = $this->input->post('course_name');
            $dataArr['course_duration'] = $this->input->post('course_duration');
            $dataArr['course_outcomes'] = $this->input->post('course_outcomes');
            $dataArr['course_desc'] = $this->input->post('course_desc');
            $dataArr['price'] = $this->input->post('price');
			$dataArr['discount'] = $this->input->post('discount');
            if(isset($_FILES['thumbnail']['tmp_name'])){
				$config['upload_path']          = './asset/img/course/';
                $config['allowed_types']        = 'jpeg|jpg|png';
                // $config['max_size']             = 100;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('thumbnail')){
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $uploadData = $this->upload->data();
                    $dataArr['thumbnail'] =  $uploadData['file_name'];
                }
		    }
			
        	if($id){
				$this->course_m->update($id, $dataArr);
        		redirect_success('admin/course',"Updated Successfully");
        	}else{
                $dataArr['created_on'] = date('Y-m-d H:i:s');
        		$this->course_m->insert($dataArr);
        		redirect_success('admin/course',"Added Successfully");
        	}
        }

        redirect_error('admin/course',"failed");
	}

	function delete(){
		$id = $this->input->post('course_id');
		$this->course_m->update($id, array("deleted"=>1));
		
        redirect_success('admin/course',"Updated Successfully");
	}

    function change_status($status, $courseID){
		$this->course_m->update($courseID, array('status'=>$status));
		
        redirect_success('admin/course',"Updated Successfully");	
	}
}