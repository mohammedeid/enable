<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'faq';
		$this->load->model('faq_m');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = "User FAQ's";
		$this->data['faqs'] = $this->faq_m->get_faq();
		
		$this->load->view('faq/index',$this->data);
	}

    public function view($id = null){
		$this->data['content'] = array();
		if($id){
			$this->data['content'] = $this->faq_m->get_faq_detail($id);
		}
		
		$this->load->view('faq/add-edit',$this->data);
	}

    function add(){
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('desc', 'Description', 'required');
       
        if ($this->form_validation->run() == TRUE){
        	$id = $this->input->post('id');
            $dataArr['title'] = $this->input->post('title');
            $dataArr['description'] = $this->input->post('desc');
            
        	if($id){
				$this->faq_m->update($id, $dataArr);
        		redirect_success('admin/faq',"Updated Successfully");
        	}else{
             $dataArr['created_on'] = date('Y-m-d H:i:s');
        		$this->faq_m->insert($dataArr);
        		redirect_success('admin/faq',"Added Successfully");
        	}
        }
        redirect_error('admin/faq',"failed");
	}

	function delete(){
		$id = $this->input->post('faq_id');
		$this->faq_m->update($id, array("deleted"=>1));
		
        redirect_success('admin/faq',"Updated Successfully");
	}
}
