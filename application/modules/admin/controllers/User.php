<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
	    $this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'user';
        $this->load->model('user_m');
		$this->load->model('roles_m');
		$this->load->model('moodle');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = 'Manage Users';
		$users = $this->user_m->get_user();

		foreach ($users as $key => $user) {
			$role = $this->roles_m->get($user->role_id);
			$user->role = $role->display_name;
		}
		$this->data['users'] = $users;
		$this->load->view('user/index',$this->data);	    
	}

	public function view($id = null){
		$this->data['user'] = array();

		$countries = get_countries();
		$this->data['countries'] = $countries;

		$roles = $this->roles_m->select('display_name,id')->get_all();
		$this->data['roles'] = array();
		$this->data['roles'][''] = 'Select Role';
		foreach ($roles as $key => $role) {
			$this->data['roles'][$role->id] = $role->display_name;
		}

		$staffadmins = $this->user_m->get_staff();
		$this->data['staff'] = array();

		$this->data['staff'][''] = 'Select Staff Admin';
		foreach ($staffadmins as $key => $staffadmin) {
			$this->data['staff'][$staffadmin->id] = $staffadmin->first_name." ".$staffadmin->last_name;
		}

		$teamadmins = $this->user_m->get_team();
		$this->data['team'] = array();

		$this->data['team'][''] = 'Select Team Admin';
		foreach ($teamadmins as $key => $teamadmin) {
			$this->data['team'][$teamadmin->id] = $teamadmin->first_name." ".$teamadmin->last_name;
		}

		if($id){
			$this->data['user'] = $this->user_m->get_user_detail($id);
		}
		
		$this->load->view('user/add-edit',$this->data);
	}  

	function add(){
		
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
        $this->form_validation->set_rules('role', 'User role', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$id = $this->input->post('id');
		if(empty($id)){
        	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_email_exists');
        }

        if ($this->form_validation->run() == TRUE){
			$firstname = $this->input->post('first_name'); 
			$lastname = $this->input->post('last_name');
        	$emailID = $this->input->post('email');
			//$password = otp_generator(6);
			$password = 'welcome';
			$username = substr($emailID, 0, strpos($emailID,"@"));
			$roleID=$this->input->post('role');
			
			$staffID=$teamID=$isAdmin='0';
			if($roleID==1){
				$isAdmin='1';
			}
			if($roleID==4){
				$staffID=$this->input->post('staff_admin');
				$teamID=$this->input->post('team_admin');
			}
			$roleData['name']= $username;
        	$roleData['first_name'] = $firstname;
			$roleData['last_name'] = $lastname;
    		$roleData['email_id'] = $emailID;
			$roleData['mobile_no'] = $this->input->post('mobile_no');
            $roleData['role_id'] = $roleID;
			$roleData['staff_id'] = $staffID;
			$roleData['team_id'] = $teamID;
			$roleData['is_admin'] = $isAdmin;
			$roleData['country'] = $this->input->post('country');
			
        	if($id){
				$this->user_m->update($id, $roleData);
        		redirect_success('admin/user',"Updated Successfully");
        	}else{
                
				$check = $this->moodle->getBy('*', array('email'=>$emailID), 'user')->row();
        		if (!$check) {
        			# code...
	                $moodle_id = $this->moodle->user_register($username, $password, $emailID, $firstname, $lastname, $data = array(), false);
	        		if ($moodle_id) {
	        			# code...
		                $to_email = $emailID;
		                $sub = 'Login Credentials | Destination Digital 2: The Digital Value Network';
		                $msg = $this->emailMsg(array('name'=>$firstname.' '.$lastname, 'uname'=>$username, 'pwd'=>$password));
		                // my_email($to_email, $sub, $msg);
		                // End here
		            	
		            	$roleData['moodle_id'] = $moodle_id;
						$roleData['password'] = getHashedPassword($password);
		            	$this->user_m->insert($roleData);
	        		}
	        		redirect_success('admin/user', "Added Successfully..!!");
	        	}else {
	        		redirect_error('admin/user', "Email already exists..!!");
	        	}
        	}
        }
        redirect_error('admin/user',"failed");
	}

	function email_exists($email) {
        $result = $this->user_m->get_by(array('email_id' => $email));
        if($result){
            $this->form_validation->set_message('check_exist', 'The %s value is already exists.');
            return false;
        }else {
            return true;
        }
    }

	function delete(){
		$id = $this->input->post('user_id');
		$this->user_m->update($id, array("deleted"=>1));
		
        redirect_success('admin/user',"Updated Successfully");
	}

	function emailMsg($data){
    	return '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html>
				<head>
					<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
					<meta content="width=device-width, initial-scale=1.0" name="viewport" />
					<style type="text/css">
						body { font-size: 16px; background: #ffffff;}
						table {border-collapse: collapse;}
						td {font-family: Arial, sans-serif; color: #363636;}
						.acc-btn{
							background-color: #433282;
							padding: 10px;
							color: #fff !important;
							text-decoration: none;
							border-radius: 3px
						}
						p{
							font-size: 14px;
						}
					</style>
				</head>
				<body>
					<table align="center" border="0" width="100%">
						<tbody>
							<tr>
								<td>
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="700">
										<tbody>
											<tr>
												<td>
													<table align="center" style="border: 1px solid #ccc;" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td>
																	<table align="center" border="0" cellpadding="0" cellspacing="0" width="700">
																		<tbody>
																			<tr>
																				<td align="center" style="padding: 15px 10px 15px 18px;" valign="middle"><a href="'.base_url().'"><img alt="Logo" src="'.base_url().'asset/images/color-logo.png" style="width: 60%;"></a>
																					<hr>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td>
																	<table align="center" border="0" cellpadding="0" cellspacing="0" width="700">
																		<tbody>
																			<tr>
																				<td align="left" valign="middle">
																					<table>
																						<tbody>
																							<tr>
																								<td align="left" style="padding: 10px 30px 20px 40px;" valign="middle">
																									<p><b>Hi '.$data['name'].'</b>,</p>
	                                                                                                <p>Thank you for signing up </p>
	                                                                                                <p>Buckle up - your exciting journey is about to begin!</p>
	                                                                                                <p>Please use the login credentials below to log onto the kenyon international platform to take your first interactive module:</p>
	                                                                                                <p>Username: '.$data['uname'].'<br>Password: '.$data['pwd'].'</p>
	                                                                                                <p><b>Best wishes,</b> <br>Kenyon International team</p>
	                                                                                                <small style="color:#ff2b2b;"><b><i>Note: This is an automated message, so please do not reply to this email.</i></b></small>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>					
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</body>
			</html>
		';
    }

}
