<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
	    $this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'ticket';
        $this->load->model('ticket_m');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = 'Manage Tickets';
        $this->data['tickets'] = $this->ticket_m->get_tickets();
		$this->load->view('ticket/index',$this->data);	    
	}

    function change_status($status, $ticketID){
		$det = array('status'=>$status);
		if($status==1){
			$det += array('close_date'=>date('Y-m-d H:i:s'));
		}
        $this->ticket_m->update($ticketID, $det);
		
        redirect_success('admin/ticket',"Updated Successfully");	
	}

	function viewTicketResponse($ticketID){
		
		$this->data['ticket_id'] = $ticketID;
		$this->data['content'] = $result = $this->ticket_m->view_ticket_response($ticketID);
     	if($result) {
            $count=0;
            foreach ($result as $key => $value) {
                if($count==0):
                    $this->data['subject'] = $value->subject;
                    $this->data['custom_id'] = $value->custom_id;
                    $this->data['description'] = $value->description;
                endif;
            }
		}

        $this->load->view('ticket/response', $this->data);
	}

	function addTicketResponse(){
		$userID = $this->user['user_id'];
		$ticketID=$this->input->post('ticket_id');
		$response=$this->input->post('ticketResponse', TRUE);
		if ($response) {
			$det = array(
						'ticket_id'=>$ticketID,
						'user_id'=>$userID,
						'msg'=>$response,
						'created_on'=>current_date()
					);
			$this->ticket_m->saveTicketResponse($det);	
		}	
		redirect("admin/ticket/viewTicketResponse/".$ticketID);	
	}

}
