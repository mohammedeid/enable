<?php defined('BASEPATH') OR exit('No direct script access allowed..!!');

class Report extends MX_Controller {
	private $data;
	function __construct() {
		# code...
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
		$this->data['pg'] = 'report';
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;  
	}

	function index(){
		$this->data['title'] = 'Reports';
		$this->load->view('report/index', $this->data);
	}

}