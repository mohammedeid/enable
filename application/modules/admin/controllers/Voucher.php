<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
	    $this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'voucher';
        $this->load->model('voucher_m');
		$this->load->model('roles_m');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->data['title'] = 'Manage Voucher Codes';
		$vouchers = $this->voucher_m->get_voucher();
		foreach ($vouchers as $key => $voucher) {
			$role = $this->roles_m->get($voucher->role_id);
			$voucher->role = $role->display_name;
		}
		$this->data['vouchers'] = $vouchers;

		$this->load->view('voucher/index',$this->data);
	}

	public function view($id = null){
		$this->data['content'] = array();
		$roles = $this->roles_m->select('display_name,id')->get_all();
		$this->data['roles'] = array();
		foreach ($roles as $key => $role) {
			$this->data['roles'][$role->id] = $role->display_name;
		}

		if($id){
			$this->data['content'] = $this->voucher_m->get_voucher_detail($id);
		}
		
		$this->load->view('voucher/add-edit',$this->data);
	}  

	function add(){
        $this->form_validation->set_rules('code', 'Code', 'required');
        $this->form_validation->set_rules('amount', 'Amount', 'required');
        $this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');

        if ($this->form_validation->run() == TRUE)
        {
        	$id = $this->input->post('id');
        	$dataArr['code'] = $this->input->post('code');
            $dataArr['coupon_amt'] = $this->input->post('amount');
            $dataArr['start_date'] = $this->input->post('start_date');
            $dataArr['end_date'] = $this->input->post('end_date');
            $dataArr['role_id'] = $this->input->post('role');
			$dataArr['usage_per_user'] = $this->input->post('usage');
			
        	if($id){
				$this->voucher_m->update($id, $dataArr);
        		redirect_success('admin/voucher',"Updated dataArr");
        	}else{
                $dataArr['created_on'] = date('Y-m-d H:i:s');
        		$this->voucher_m->insert($dataArr);
        		redirect_success('admin/voucher',"Added Successfully");
        	}
        }

        redirect_error('admin/voucher',"failed");
	}

	function delete(){
		$id = $this->input->post('voucher_id');
		$this->voucher_m->update($id, array("deleted"=>1));
		
        redirect_success('admin/voucher',"Updated Successfully");
	}

    function change_status($status, $voucherID){
		$this->voucher_m->update($voucherID, array('status'=>$status));
		
        redirect_success('admin/voucher',"Updated Successfully");	
	}
}