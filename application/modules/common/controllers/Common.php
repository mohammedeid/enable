<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Common extends MX_Controller{

  function __construct(){
    parent::__construct();
  }

  function header($data = NULL){
    $data['logged_user'] =  require_user();
    $this->load->view('header', $data);
  } 

  function footer($data = NULL){ 
    $logged_user =  require_user();
    $this->load->view('footer', $data);
  }

  function login_header($data = NULL){
    $this->load->view('login_header', $data);
  }

  function login_footer($data = NULL){
    $this->load->view('login_footer', $data);
  }

  function menu($data = NULL){
    $logged_user =  require_user();
    $this->load->model('settings_m');
    $data['setting'] = $this->settings_m->get(1);
    $this->load->view('menu', $data);
  }

}
/*  EOF */