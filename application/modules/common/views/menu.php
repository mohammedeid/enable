<!-- <div class="main-content app-content"> -->
<div class="main-header sticky side-header nav nav-item">
   <div class="container-fluid">
      <div class="main-header-left ">
         <div class="responsive-logo">
            <a href="index.html"><img src="<?=base_url();?>asset/img/brand/logo.png" class="logo-1" alt="logo"></a>
            <a href="index.html"><img src="<?=base_url();?>asset/img/brand/logo-white.png" class="dark-logo-1" alt="logo"></a>
            <a href="index.html"><img src="<?=base_url();?>asset/img/brand/favicon.png" class="logo-2" alt="logo"></a>
            <a href="index.html"><img src="<?=base_url();?>asset/img/brand/favicon.png" class="dark-logo-2" alt="logo"></a>
         </div>
         <div class="app-sidebar__toggle" data-toggle="sidebar">
            <a class="open-toggle" href="#"><i class="header-icon fe fe-align-left" ></i></a>
            <a class="close-toggle" href="#"><i class="header-icons fe fe-x"></i></a>
         </div>
      </div>
      <div class="main-header-right">
         <div class="nav nav-item  navbar-nav-right ml-auto">
            <div class="nav-item welcome-msg">Welcome <?php echo $user['first_name']." ".$user['last_name']; ?></div>
            <!-- <div class="nav-item full-screen fullscreen-button">
               <a class="new nav-link full-screen-link" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="header-icon-svgs" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-maximize"><path d="M8 3H5a2 2 0 0 0-2 2v3m18 0V5a2 2 0 0 0-2-2h-3m0 18h3a2 2 0 0 0 2-2v-3M3 16v3a2 2 0 0 0 2 2h3"></path></svg></a>
            </div> -->
         </div>
      </div>
   </div>
</div>
   <?php 
      if($user['role_id']==1){
         $homeUrl=base_url('admin');
      } elseif($user['role_id']==2){
         $homeUrl=base_url('staff');
      } elseif($user['role_id']==3){
         $homeUrl=base_url('team');
      } elseif($user['role_id']==4){
         $homeUrl=base_url('user');
      }

   ?>
<aside class="app-sidebar sidebar-scroll clearfix ">
   <div class="main-sidebar-header active">
      <a class="desktop-logo logo-light active" href="<?=$homeUrl;?>">
         <img src="<?=base_url('uploads/'.$setting->logo);?>" class="main-logo" alt="logo">
      </a>
      <a class="logo-icon mobile-logo icon-light active" href="<?=$homeUrl;?>">
         <img src="<?=base_url();?>asset/img/brand/favicon.png" class="logo-icon" alt="logo">
      </a>
   </div>
   <div class="main-sidemenu">
      <ul class="side-menu">
         <?php if($user['role_id']==1){?>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('admin');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Dashboard</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('admin/course');?>">
               <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Manage Courses</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'voucher')?'active':'';?>" href="<?=base_url('admin/voucher');?>">
               <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Manage Voucher Codes</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'user')?'active':'';?>" href="<?=base_url('admin/user');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-users side-menu__icon" viewBox="0 0 24 24"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  <span class="side-menu__label">Manage Users</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'forum')?'active':'';?>" href="<?=base_url('admin/forum');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Manage Forums</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'report')?'active':'';?>" href="<?=base_url('admin/report');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M19 5H5v14h14V5zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z" opacity=".3"></path><path d="M3 5v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2zm2 0h14v14H5V5zm2 5h2v7H7zm4-3h2v10h-2zm4 6h2v4h-2z"></path></svg>
                  <span class="side-menu__label">Reports</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'ticket')?'active':'';?>" href="<?=base_url('admin/ticket');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Manage Tickets</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'setting')?'active':'';?>" href="<?=base_url('admin/setting');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Site Settings</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'changepassword')?'active':'';?>" href="<?=base_url('profile/changePassword');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Change Password</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('login/logout');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Log Out</span>
               </a>
            </li>
         <?php } elseif($user['role_id']==2){?>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'dashboard')?'active':'';?>" href="<?=base_url('staff');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Dashboard</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'user')?'active':'';?>" href="<?=base_url('staff/user');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-users side-menu__icon" viewBox="0 0 24 24"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  <span class="side-menu__label">Users</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'report')?'active':'';?>" href="<?=base_url('staff/report');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M19 5H5v14h14V5zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z" opacity=".3"></path><path d="M3 5v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2zm2 0h14v14H5V5zm2 5h2v7H7zm4-3h2v10h-2zm4 6h2v4h-2z"></path></svg>
                  <span class="side-menu__label">Reports</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'changepassword')?'active':'';?>" href="<?=base_url('profile/changePassword');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Change Password</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('login/logout');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Log Out</span>
               </a>
            </li>
         <?php } elseif($user['role_id']==3){?> 
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'dashboard')?'active':'';?>" href="<?=base_url('team');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Dashboard</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'user')?'active':'';?>" href="<?=base_url('team/user');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-users side-menu__icon" viewBox="0 0 24 24"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  <span class="side-menu__label">Users</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'report')?'active':'';?>" href="<?=base_url('team/report');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M19 5H5v14h14V5zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z" opacity=".3"></path><path d="M3 5v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2zm2 0h14v14H5V5zm2 5h2v7H7zm4-3h2v10h-2zm4 6h2v4h-2z"></path></svg>
                  <span class="side-menu__label">Reports</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'changepassword')?'active':'';?>" href="<?=base_url('profile/changePassword');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Change Password</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('login/logout');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Log Out</span>
               </a>
            </li>
         <?php } elseif($user['role_id']==4){?>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'dashboard')?'active':'';?>" href="<?=base_url('user');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Dashboard</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'mycourse')?'active':'';?>" href="<?=base_url('user/mycourse');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">My Course</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'purchasecourse')?'active':'';?>" href="<?=base_url('user/purchasecourse');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Purchase Course</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'calender')?'active':'';?>" href="<?=base_url('user/calender');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">Calender</span>
               </a>
            </li>
            <li  class="slide">
               <a class="side-menu__item <?=($menu_active === 'myticket')?'active':'';?>" href="<?=base_url('user/ticket');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">My Ticket's </span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'faq')?'active':'';?>" href="<?=base_url('user/faq');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v4H5zm10 10h4v4h-4zM5 15h4v4H5zM16.66 4.52l-2.83 2.82 2.83 2.83 2.83-2.83z" opacity=".3"></path><path d="M16.66 1.69L11 7.34 16.66 13l5.66-5.66-5.66-5.65zm-2.83 5.65l2.83-2.83 2.83 2.83-2.83 2.83-2.83-2.83zM3 3v8h8V3H3zm6 6H5V5h4v4zM3 21h8v-8H3v8zm2-6h4v4H5v-4zm8-2v8h8v-8h-8zm6 6h-4v-4h4v4z"></path></svg>
                  <span class="side-menu__label">FAQ's</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item <?=($menu_active === 'changepassword')?'active':'';?>" href="<?=base_url('profile/changePassword');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Change Password</span>
               </a>
            </li>
            <li class="slide">
               <a class="side-menu__item" href="<?=base_url('login/logout');?>">
                  <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3"></path><path d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z"></path></svg>
                  <span class="side-menu__label">Log Out</span>
               </a>
            </li>
         <?php }?>   
      </ul>
   </div>
</aside>
