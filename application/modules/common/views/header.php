<!doctype html>
<html lang="en" dir="ltr">
   <head>

      <meta charset="UTF-8">
      <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="Description" content="">
      <meta name="Author" content="">
      <meta name="Keywords" content=""/>

      <!-- Title -->
      <title>kenyon</title>
      <!-- Bootstrap Core CSS -->
      <link href="<?=base_url();?>asset/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <!-- Favicon -->
      <link rel="icon" href="<?=base_url();?>asset/img/brand/favicon.png" type="image/x-icon"/>

      <!-- Icons css -->
      <link href="<?=base_url();?>asset/css/icons.css" rel="stylesheet">

      <!--  Right-sidemenu css -->
      <link href="<?=base_url();?>asset/plugins/sidebar/sidebar.css" rel="stylesheet">

      <!--  Custom Scroll bar-->
      <link href="<?=base_url();?>asset/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>

      <!--Internal  Datetimepicker-slider css -->
      <link href="<?=base_url();?>asset/plugins/amazeui-datetimepicker/css/amazeui.datetimepicker.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/plugins/pickerjs/picker.min.css" rel="stylesheet">

      <!-- Internal Data table css -->
      <link href="<?=base_url();?>asset/plugins/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
      <link href="<?=base_url();?>asset/plugins/datatable/css/buttons.bootstrap4.min.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/plugins/datatable/css/responsive.bootstrap4.min.css" rel="stylesheet" />
      <link href="<?=base_url();?>asset/plugins/datatable/css/jquery.dataTables.min.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/plugins/datatable/css/responsive.dataTables.min.css" rel="stylesheet">
      <!-- Internal Select2 css -->
      <link href="<?=base_url();?>asset/plugins/select2/css/select2.min.css" rel="stylesheet">   
      <!-- Internal Bootstrap-Duallist table css -->
      <link href="<?=base_url();?>asset/plugins/bootstrap-duallist/bootstrap-duallistbox.min.css" rel="stylesheet" />
      <!-- Internal fullcalendar Css-->
      <link href="<?=base_url();?>asset/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet">
      
      <!--  Left-Sidebar css -->
      <link rel="stylesheet" href="<?=base_url();?>asset/css/sidemenu.css">


      <!--- Style css --->
      <link href="<?=base_url();?>asset/css/style.css" rel="stylesheet">

      <!--- Dark-mode css --->
      <link href="<?=base_url();?>asset/css/style-dark.css" rel="stylesheet">

      <!---Skinmodes css-->
      <link href="<?=base_url();?>asset/css/skin-modes.css" rel="stylesheet" />



      <!--- Animations css-->
      <link href="<?=base_url();?>asset/css/animate.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/custom/custom.css" rel="stylesheet">
      <link href="<?=base_url();?>asset/css/custom-accordion.css" rel="stylesheet">

      <script type="text/javascript">
         var baseUrl = '<?=base_url();?>';
      </script>
   </head>
   <body class="main-body app sidebar-mini">
      <div id="global-loader">
         <img src="<?=base_url();?>asset/img/loader.svg" class="loader-img" alt="Loader">
      </div>

      <div class="page">

         <div class="main-content app-content">
            <?=modules::run('common/menu');?>
     