		</div>
	</div>
	<a href="#top" id="back-to-top"><i class="las la-angle-double-up"></i></a>
	<script src="<?=base_url();?>asset/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Bundle js -->
	<script src="<?=base_url();?>asset/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Ionicons js -->
	<script src="<?=base_url();?>asset/plugins/ionicons/ionicons.js"></script>

	<!-- Moment js -->
	<script src="<?=base_url();?>asset/plugins/moment/moment.js"></script>

	<!--Internal  Fullcalendar js -->
	<script src="<?=base_url();?>asset/plugins/fullcalendar/fullcalendar.min.js"></script>

	<!-- P-scroll js -->
	<script src="<?=base_url();?>asset/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/perfect-scrollbar/p-scroll.js"></script>

	<!-- Sticky js -->
	<script src="<?=base_url();?>asset/js/sticky.js"></script>

	<!-- eva-icons js -->
	<script src="<?=base_url();?>asset/js/eva-icons.min.js"></script>

	<!-- Rating js-->
	<script src="<?=base_url();?>asset/plugins/rating/jquery.rating-stars.js"></script>
	<script src="<?=base_url();?>asset/plugins/rating/jquery.barrating.js"></script>

	<!-- Custom Scroll bar Js-->
	<script src="<?=base_url();?>asset/plugins/mscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

	<!-- Sidebar js -->
	<script src="<?=base_url();?>asset/plugins/side-menu/sidemenu.js"></script>

	<!-- Right-sidebar js -->
	<script src="<?=base_url();?>asset/plugins/sidebar/sidebar.js"></script>
	<script src="<?=base_url();?>asset/plugins/sidebar/sidebar-custom.js"></script>

	<!-- Internal Data tables -->
	<script src="<?=base_url();?>asset/plugins/datatable/js/jquery.dataTables.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/dataTables.dataTables.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/dataTables.responsive.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/responsive.dataTables.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/jquery.dataTables.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/dataTables.bootstrap4.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/dataTables.buttons.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/buttons.bootstrap4.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/jszip.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/pdfmake.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/vfs_fonts.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/buttons.html5.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/buttons.print.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/buttons.colVis.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/dataTables.responsive.min.js"></script>
	<script src="<?=base_url();?>asset/plugins/datatable/js/responsive.bootstrap4.min.js"></script>

	<!-- Internal Select2.min js -->
	<script src="<?=base_url();?>asset/plugins/select2/js/select2.min.js"></script>


	<!--Internal  jquery-simple-datetimepicker js -->
	<script src="<?=base_url();?>asset/plugins/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js"></script>

	<!-- Ionicons js -->
	<script src="<?=base_url();?>asset/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.js"></script>

	<!--Internal  pickerjs js -->
	<script src="<?=base_url();?>asset/plugins/pickerjs/picker.min.js"></script>

	<!-- Internal Bootstrap-Duallist table js -->
    <script src="<?=base_url();?>asset/plugins/bootstrap-duallist/jquery.bootstrap-duallistbox.min.js"></script>

    <!--Internal  Chart.bundle js -->
	<script src="<?=base_url();?>asset/plugins/chart.js/Chart.bundle.min.js"></script>

    <!-- tinymce -->
    <script src="https://cdn.tiny.cloud/1/lwiiwq37palkdjwr3xt0qznvfrh1z81vv76ch8w9qu362yg5/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

	<!-- custom js -->
	<script src="<?=base_url();?>asset/js/custom.js"></script>

	<?if (!empty($pg)){?>
	    <script src="<?=get_asset('custom/js', $pg.'.js') ?>"></script>
	<?}?>
</body>
</html>