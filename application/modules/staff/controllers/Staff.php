<?php defined('BASEPATH') OR exit('No direct access allowed..!!');

class Staff extends MX_Controller {
	private $data;
	function __construct() {
		# code...
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
		$this->data['pg'] = 'staff';
		$this->load->model('user_m');
		$this->load->library('form_validation');
  		$this->form_validation->CI =& $this; 
	}

    function index(){
		$this->data['menu_active'] = 'dashboard';
		$this->data['title'] = 'Dashboard';
		$this->load->view('dashboard/index', $this->data);
	}

	function user(){
		$this->data['menu_active'] = 'user';
		$this->data['title'] = 'Users';
		$staff_id=$this->user['user_id'];
		$this->data['users'] = $this->user_m->get_staff_user($staff_id);
		$this->load->view('user/index', $this->data);
	}

	function report(){
		$this->data['menu_active'] = 'report';
		$this->data['title'] = 'Reports';
		$this->load->view('report/index', $this->data);
	}
}