<?php defined('BASEPATH') OR exit('No direct access allowed..!!');

class User extends MX_Controller {
	private $data;
	function __construct() {
		# code...
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
		$this->data['pg'] = 'user';
		$this->load->model('faq_m');
		$this->load->library('form_validation');
  		$this->form_validation->CI =& $this; 
	}

    function index(){
		$this->data['menu_active'] = 'dashboard';
		$this->data['title'] = 'Dashboard';
		$this->load->view('dashboard/index', $this->data);
	}

	function faq(){
		$this->data['menu_active'] = 'faq';
		$this->data['title'] = "FAQ's";
		$this->data['content'] = $this->faq_m->get_many_by(array('status'=>'0','deleted'=>'0'));
		$this->load->view('faq/index', $this->data);
	}

}