<?php defined('BASEPATH') OR exit('No direct access allowed..!!');

class Ticket extends MX_Controller {
	private $data;
	function __construct() {
		# code...
		parent::__construct();
		$this->data['user'] = $this->user = require_user();
		$this->data['pg'] = 'ticket';
		$this->data['menu_active'] = 'myticket';
		$this->load->model('ticket_m');
		$this->load->library('form_validation');
  		$this->form_validation->CI =& $this; 
	}

    function index(){
		$this->data['title'] = " My Ticket's";
        $user_id=$this->user['user_id'];
        $this->data['tickets'] = $this->ticket_m->get_user_tickets($user_id);

		$this->load->view('ticket/index', $this->data);
	}

    public function view(){
		$this->data['user_id'] = $this->user['user_id'];

		$this->load->view('ticket/add',$this->data);
	}

    function add(){
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
       
        if ($this->form_validation->run() == TRUE){
			$cnt = $this->ticket_m->getCount('ticket');
        	$dataArr['user_id'] = $this->input->post('user_id');
			$dataArr['custom_id'] = 'TICKT-'.sprintf("%03d", ($cnt+1));
        	$dataArr['subject'] = $this->input->post('subject');
            $dataArr['description'] = $this->input->post('description');
			$dataArr['created_on'] = date('Y-m-d H:i:s');

			$this->ticket_m->insert($dataArr);
			redirect_success('user/ticket',"Added Successfully");
        	
        }

        redirect_error('user/ticket',"failed");
	}

	function viewTicketResponse($ticketID){
		$this->data['ticket_id'] = $ticketID;
		$this->data['content'] = $result = $this->ticket_m->view_ticket_response($ticketID);
     	if($result) {
            $count=0;
            foreach ($result as $key => $value) {
                if($count==0):
                    $this->data['subject'] = $value->subject;
                    $this->data['custom_id'] = $value->custom_id;
                    $this->data['description'] = $value->description;
                endif;
            }
		}

        $this->load->view('user/ticket/response', $this->data);
	}

	function addTicketResponse(){
		$userID = $this->user['user_id'];
		$ticketID=$this->input->post('ticket_id');
		$response=$this->input->post('ticketResponse', TRUE);
		if ($response) {
			$det = array(
						'ticket_id'=>$ticketID,
						'user_id'=>$userID,
						'msg'=>$response,
						'created_on'=>date('Y-m-d H:i:s')
					);
			$this->ticket_m->saveTicketResponse($det);	
		}	
		redirect("user/ticket/viewTicketResponse/".$ticketID);	
	}

    

}