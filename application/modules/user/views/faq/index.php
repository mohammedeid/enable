<?=modules::run('common/header');?>
<div class="container-fluid">
	<!-- Page Breadcrumb -->
	<div class="breadcrumb-header justify-content-between">
		<div class="my-auto">
			<div class="d-flex">
				<h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
			</div>
		</div>
	</div>
    <div class="col-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php if($content) {
                                    foreach ($content as $key => $value) {
                                # code...
                            ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading<?=$value->id;?>">
                                                <h4 class="panel-title mb-0">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$value->id;?>" aria-expanded="false" aria-controls="collapse<?=$value->id;?>" class="collapsed">
                                                    <?=$value->title;?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?=$value->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$value->id;?>">
                                                <div class="panel-body"><?=$value->description;?></div>
                                            </div>
                                        </div>
                            <?php   }
                                    }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>
<script type="text/javascript">
    function toggleClass(id){
        $('i').toggleClass('fa-minus-circle fa-plus-circle');
        $("#item-head"+id+' > .timeline-heading').toggleClass('content-hide content-show');
        $("#item-head"+id).toggleClass('custom-item-lines');
        $("#item-head"+id+' > hr').toggleClass('content-hide content-show');
    }
</script>