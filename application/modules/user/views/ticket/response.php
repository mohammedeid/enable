<?=modules::run('common/header');?>
<div class="container-fluid">
    <!-- Page Breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">Ticket Details</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body">
                <div class="row">
					<div class="col-md-12">
						<div class='ticket-holder'>
							<div class='subject-holder'><?=$subject;?><span class='user-title'> (<?=$custom_id;?>)</span></div>
							<div class='desc-holder'><?=$description;?></div>
							<div>
								<?=form_open('user/ticket/addTicketResponse','class="form-horizontal"');?>
									<input type="hidden" name="ticket_id" value="<?=$ticket_id;?>">
									<div class="row">
										<div class="col-md-12">
											<textarea id="ticketResponse" name="ticketResponse" class="form-control"></textarea>
                                        </div>
									</div>
									<div class="row">	
										<div class="col-md-12">
											<button class="btn btn-primary btn-reply">REPLY</button>
										</div>
									</div>
								<?=form_close();?>
							</div>	
						</div>
                        <?php
                        $row='';
                        if($content) {
                            $count=0;
                            foreach ($content as $key => $value) {
                                if($value->msg) {	
                                    if($count==0):
                                    $row.="<div class='comment-container'>"
                                        ."	    <div class='comment-title'>Comments</div>";
                                    endif;	
                                    if($value->image_name):
                                        $imageUrl=base_url()."asset/upload/".$value->image_name;
                                    else:
                                        $imageUrl=base_url()."asset/img/no_img.jpg";
                                    endif;		
                                    	
                                    $row.="<div class='comment-holder'>"
                                        ."	<div class='image-holder'>"
                                        ."		<img src='".$imageUrl."'  alt=''>"
                                        ."	</div>"
                                        ."	<div class='name-holder'>"
                                        ."		<div class='name-title'>".$value->reply_name."</div>"
                                        ."		<div class='date-title'>".date('d-M-Y',strtotime($value->reply_date))."</div>"
                                        ."	</div>"
                                        ."	<div class='message-holder'>".$value->msg."</div>"
                                        ."</div>";
                                    $count++;
                                }
                            }
				            $row.="	</div>";
			            }
                        echo $row;
                        ?>
					</div> 
                </div>
            </div>
        </div>
    </div>
</div>
<?=modules::run('common/footer');?>   
<script>
    tinymce.init({
		selector:'textarea[name="ticketResponse"]',
		menubar: false,
	});
</script>     