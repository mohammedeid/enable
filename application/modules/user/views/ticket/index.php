<?=modules::run('common/header');?>
<div class="container-fluid">
	<!-- Page Breadcrumb -->
	<div class="breadcrumb-header justify-content-between">
		<div class="my-auto">
			<div class="d-flex">
				<h4 class="content-title mb-0 my-auto"><?=$title;?></h4>
			</div>
		</div>
        <div class="d-flex my-xl-auto right-content">
            <div class="pr-1 mb-3 mb-xl-0">
                <a href="#" data-load-url="<?=base_url();?>user/ticket/view" data-toggle="modal" data-target="#addEditModal" class="btn btn-icon btn-primary btn-sm mr-2">
                <i class="fe fe-plus"></i>
                </a>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-md-12">
         <!-- Page Content here -->
            <div class="card card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="user_ticket">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center">#</th>
                                <th>Ticket ID</th>
                                <th>Subject</th>
                                <th>Status</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if($tickets){
                                $i=0;
                                foreach($tickets as $value) {
                                    # code...
                                    $ticketStatus=get_ticket_status($value->status);
                                    if($value->status==0){
                                        $status = "<span class='text-danger'>".$ticketStatus."</span>";
                                    } else {
                                        $status = "<span class='text-success'>".$ticketStatus."</span>";
                                    }
                                    $ticket_no = "<a href='".base_url()."user/ticket/viewTicketResponse/".$value->id."' >".$value->custom_id."</a>";
                                ?>
                                <tr>
                                    <td class="text-center"><?=++$i;?></td>
                                    <td><?=$ticket_no;?></td>
                                    <td><?=$value->subject;?></td>
                                    <td><?=$status;?></td>
                                    <td class="text-center"><?=date('d-M-y H:i A',strtotime($value->created_on));?></td>
                                </tr>
                                <?php
                                }
                            }
                        ?>
                    </tbody>
               </table>
            </div>
         </div>
        </div>
    </div>
</div>
<div class="modal effect-scale" id="addEditModal" data-backdrop="static">
   <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content"></div>
   </div>
</div>

<?=modules::run('common/footer');?>