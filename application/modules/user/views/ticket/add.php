<?=form_open_multipart('user/ticket/add');?>
<input type="hidden" name="user_id" id="user_id" value="<?=$user_id?>">
<div class="modal-header">
   <h5 class="modal-title" id="exampleModalLabel"><?="Add Ticket";?></h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="subject">Subject <span class="text-danger">*</span></label>
                <input type="text" name="subject" class="form-control" id="subject" value="" required placeholder="Enter subject">
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="description">Description <span class="text-danger">*</span></label>
                <textarea name="description" class="form-control" rows="10" id="description" ></textarea>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Submit</button>
</div>
<?=form_close();?>

