<?=modules::run('common/header');?>
<div class="container-fluid">
	<!-- Page Breadcrumb -->
	<div class="breadcrumb-header justify-content-between">
		<div class="my-auto">
			<div class="d-flex">
				<h4 class="content-title mb-0 my-auto">Change Password</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<!-- Page Content here -->
				<div class="card card-body">
				    <div class="col-6">
				        <?php echo form_open('profile/changePassword', array('id' => 'passwordForm'))?>
				            <div class="form-group">
				                <input type="password" name="oldpass" id="oldpass" class="form-control" placeholder="Old Password" required="required" />
				                <?php echo form_error('oldpass', '<div class="error">', '</div>')?>
				            </div>
				            <div class="form-group">
				                <input type="password" name="newpass" id="newpass" class="form-control" placeholder="New Password" required="required"  />
				                <?php echo form_error('newpass', '<div class="error">', '</div>')?>
				            </div>
				            <div class="form-group">
				                <input type="password" name="passconf" id="passconf" class="form-control" placeholder="Confirm Password" required="required"  />
				                <?php echo form_error('passconf', '<div class="error">', '</div>')?>
				            </div>
				            <div class="form-group">
				                <button type="submit" class="btn btn-success">Change Password</button>
				                <button type="reset" class="btn btn-danger">Reset</button>
				            </div>
				        <?php echo form_close(); ?>
				    </div>
				</div>
			<!-- Page Content end here -->
		</div>
	</div>
</div>
<?=modules::run('common/footer');?>