<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {
	private $data;
	public function __construct(){
		parent::__construct();
	    $this->data['user'] = $this->user = require_user();
	    $this->data['pg'] = 'profile';
        $this->data['menu_active'] = 'changepassword';
		$this->load->model('user_m');
        $this->load->model('moodle');
        $this->load->library('form_validation');
 		$this->form_validation->CI =& $this;     
	}
	
	public function index() {
		$this->load->view('index');	    
	}

    public function changePassword() {

        $this->data['title'] = 'Change Password';
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldpass', 'old password', 'callback_password_check');
        $this->form_validation->set_rules('newpass', 'new password', 'required');
        $this->form_validation->set_rules('passconf', 'confirm password', 'required|matches[newpass]');

        $this->form_validation->set_error_delimiters('<div class="error text-danger">', '</div>');

        if($this->form_validation->run() == false) {
            $this->load->view('profile/change-password', $this->data);
        }
        else {

            $newpass = $this->input->post('newpass');
            $this->moodle->change_password($this->user['moodle_id'], $newpass);

            redirect('login/logout');

        }
    }

    function password_check($oldpass){
        $result = $this->moodle->check_password($this->user['moodle_id'], $oldpass);
        if($result == false) {
            $this->form_validation->set_message('password_check', 'The {field} does not match');
            return false;
        }
        return true;
    }

	



}
