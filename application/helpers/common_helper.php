<?php
/* ------------------------------ Date Helper ------------------------------ */
function current_date()
{
  return date('Y-m-d H:i:s');
}

function get_display_date($datetimeFromMysql)
{
  return date("d-m-Y H:i:s", strtotime($datetimeFromMysql));
}

function get_display_time($datetimeFromMysql)
{
  return date("H:i:s", strtotime($datetimeFromMysql));
}

function current_date_only($date)
{
  return date("Y-m-d", strtotime($date));
}

function add_days($days, $date)
{

  $date = strtotime("+" . $days . " days", strtotime($date));
  return date("d/m/Y", $date);
}

function date_for_mysql($date)
{
  list ($d, $m, $y) = explode("-", $date);
  return $y . "-" . $m . "-" . $d;
}

function date_for_php($date)
{
  list ($y, $m, $d) = explode("-", $date);
  return $d . "-" . $m . "-" . $y;
}


function email_date_format($date)
{
  return date("d-M-Y", strtotime($date));
}

function dashboard_date_format($date)
{
  return date("H:i d-M-y", strtotime($date));
}

function ticket_date_format($date)
{
  return date("d-M-y H:i", strtotime($date));
}

function log_email_date()
{
  return date('d-m-Y H:i:s');
}
/* ------------------------------ Session	+ Site ------------------------------ */
function get_asset($type, $file, $path = 'base', $version = TRUE)
{
  $out = base_url() . 'asset/';
  $out .= $type . '/';
  if ($path != 'base')
    $out .= $path . '/';

  $out .= $file;

  if ($version)
    $out .= '?v=' . get_version();

  return $out;
}

function get_version()
{
  $CI =& get_instance();
  return $CI->config->item('version');
}


function format_for_view($str, $limit = 220, $break = " ", $camel = false, $pad = " ...")
{
  $format_str = '';
  // return with no change if string is shorter than $limit
  if ($limit < 0 || strlen($str) <= $limit) {
    $format_str = $str;
  } else {
    $str = strip_tags($str);
    $format_str = substr($str, 0, ($limit - strlen($pad))) . $pad;
  }

  if ($camel == true) {
    return ucwords($format_str);
  } else {
    return ucfirst($format_str);
  }
}


function array_iunique($topics)
{

  $ltopics = array_map('strtolower', $topics);
  $cleanedTopics = array_unique($ltopics);

  foreach ($topics as $key => $value) {
    if (!isset($cleanedTopics[$key])) {
      unset($topics[$key]);
    }
  }

  return $topics;

}


function get_session_time_zone()
{
  $CI = &get_instance();
  $user = $CI->session->userdata('ebridge_user');
  if (!empty($user)) {
    return $user['time_zone'];
  } else {
    return 'GMT';
  }
}


function redirect_success($path, $message)
{
  $CI = &get_instance();
  $CI->session->set_flashdata('success', $message);
  redirect($path);
}

function set_success()
{
  $CI = &get_instance();
  $CI->session->set_flashdata('success', true);
}

function get_success()
{
  $CI = &get_instance();
  $success = $CI->session->flashdata('success');
  return $success ? true : false;

}

function redirect_notice($path, $message)
{
  $CI = &get_instance();
  $CI->session->set_flashdata('notice', $message);
  redirect($path);
}

function redirect_error($path, $message)
{
  $CI = &get_instance();
  $CI->session->set_flashdata('error', $message);
  redirect($path);
}

function show_message()
{
  $CI = &get_instance();
  $success = $CI->session->flashdata('success');
  $notice = $CI->session->flashdata('notice');
  $error = $CI->session->flashdata('error');

  $html = '';

  if (!empty($success))
    $html .= "<div class='alert alert-success alert-dismissible alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$success</div>";
  if (!empty($notice))
    $html .= "<div class='alert alert-warning alert-dismissible alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$notice</div>";
  if (!empty($error))
    $html .= "<div class='alert alert-warning alert-danger alert-gap notification' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>$error</div>";

  return $html;
}

function require_user($redirect = true)
{
  $CI = &get_instance();
  #print_r($CI->session->userdata()); exit;
  $user = $CI->session->userdata('kenyon_lms_userdata');
  if (empty($user) && $redirect) {
    if ($CI->input->is_ajax_request()) {
      $CI->output->set_status_header('302');
    } else {
      $redirect = '';
      $segs = $CI->uri->segment_array();
      foreach ($segs as $segment) {
        $redirect .= '/' . $segment;
      }
      $CI->session->set_userdata('redirect', $redirect);
      redirect("login");
    }
  } else {
    return $user;
  }
}


function initialize_admin()
{
  $CI = &get_instance();
  $CI->load->library('session');
}

function str_replace_once($str_pattern, $str_replacement, $string)
{
  if (strpos($string, $str_pattern) !== false) {
    $occurrence = strpos($string, $str_pattern);
    return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
  }
  return $string;
}


/* ************** End of Admin Panel ****/
function get_user_time_zone()
{
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
    $ipaddress = getenv('HTTP_CLIENT_IP');
  else if (getenv('HTTP_X_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if (getenv('HTTP_X_FORWARDED'))
    $ipaddress = getenv('HTTP_X_FORWARDED');
  else if (getenv('HTTP_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if (getenv('HTTP_FORWARDED'))
    $ipaddress = getenv('HTTP_FORWARDED');
  else if (getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
  else
    $ipaddress = 'UNKNOWN';

  #$ipaddress='4.255.255.255';
  #echo $ipaddress;

  if ($ipaddress == 'UNKNOWN') {
    return 'GMT';
  } else {
    $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ipaddress));
    if ($query && $query['status'] == 'success') {
      return $query['timezone'];
    } else {
      return 'GMT';
    }
  }
  //$ip = $_REQUEST['REMOTE_ADDR']; // the IP address to query
}

function my_send_email_old($recipient, $bcc_recipient = null, $sender_name, $sender_email, $reply_to, $subject, $message, $attachments = NULL, $delay = false, $alt_message = NULL)
{
  $CI =& get_instance();

  $config = array(
    'mailtype' => 'html',
    'charset' => 'utf-8',
    'newline' => '\r\n',
    /* 'bcc_batch_mode' => true,
     'bcc_batch_size' => 1*/
  );

  $CI->load->library('email', $config);

  // $CI->load->library('email');

  $CI->email->initialize();

  $CI->email->from($sender_email, $sender_name);
  $CI->email->to($recipient);
  $CI->email->reply_to($reply_to, $sender_name);

  if ($bcc_recipient) {
    $CI->email->bcc($bcc_recipient);
  }

  $CI->email->subject($subject);
  $CI->email->message($message);
  if ($alt_message) {
    $CI->email->set_alt_message($alt_message);
  } else {
    $CI->email->set_alt_message(strip_tags($message));
  }

  if ($attachments)
    foreach ($attachments as $attachment)
      $CI->email->attach($attachment->path . $attachment->file_name);

  if ($delay)
    sleep($delay);

  $CI->email->send();
  #echo $CI->email->print_debugger();exit;
  if ($CI->email->send())
    return true;
  else
    return false;
}

function my_send_email($recipient, $bcc_recipient = null, $sender_name, $sender_email, $reply_to, $subject, $message, $attachments = NULL, $delay = false, $alt_message = NULL){
  $CI =& get_instance();
  $config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'smtp.mailgun.org',
    'smtp_port' => 587,
    'smtp_user' => 'postmaster@ebridge.eidesign.net',
    'smtp_pass' => '6bc59ddaee1987ee6d425f2485f29ad1-6f4beb0a-b21f4f31',
    'mailtype'  => 'html', 
    'charset'   => 'utf-8'
  );
  $CI->load->library('email', $config);
  $CI->email->set_newline("\r\n");

  // Set to, from, message, etc.
  $CI->email->from('admin@eidesign.net', 'eBridge Admin');
  $CI->email->to($recipient);
  $CI->email->reply_to($reply_to, $sender_name);
  if ($bcc_recipient) {
    $CI->email->bcc($bcc_recipient);
  }

  $CI->email->subject($subject);
  $CI->email->message($message);
  
  if ($attachments){
    foreach ($attachments as $attachment){
      $CI->email->attach($attachment->path . $attachment->file_name);
    }
  }

  $CI->email->send();
  if ($CI->email->send())
    return true;
  else
    return false;
}

function get_random()
{
  return md5(uniqid());
}

function application_type($index = null)
{
  $types = array('productivity', 'non-productivity', 'unknown', 'desktop', 'system', 'idle', 'away');
  return isset($index) ? $types[$index] : $types;
}

function away_type($index = null)
{
  $types = array('away', 'idle');
  return isset($index) ? $types[$index] : $types;
}

function browser_type($index = null)
{
  $types = array('non-productivity');
  return isset($index) ? $types[$index] : $types;
}

function get_base_dir()
{
  $CI =& get_instance();
  return $CI->config->item('base_dir');
}

//*  EOF */

function get_milestone_status($index = null)
{
  $status = array('', 'Yes', 'WIP', 'High Risk', 'Inactive', 'Low Risk');
  return isset($index) ? $status[$index] : $status;
}

function get_milestone_coll_status($index = null)
{
  $status = array('', 'Yes', 'No');
  return isset($index) ? $status[$index] : $status;
}

function get_milestone_type_status($index = null)
{
  $status = array('', 'OF', 'Int.');
  return isset($index) ? $status[$index] : $status;
}

function get_outsource_status($index = null)
{
  $status = array('', 'Yes', 'WIP', 'Paid', 'Hold', 'Invoice received');
  return isset($index) ? $status[$index] : $status;
}

function get_outsource_type_status($index = null)
{
  $status = array('', 'VO', 'Trans', 'VD Int', 'VD Ext', 'ID Int', 'ID Ext', 'Infra', 'Contract', 'Tech');
  return isset($index) ? $status[$index] : $status;
}

function get_reason_for_delay($index = null)
{
  $status = array('', 'Customer delay', 'Internal reworks', 'Estimation error');
  return isset($index) ? $status[$index] : $status;
}

function get_coll_days($index = null)
{
  $status = array('', 5, 15, 30, 45, 60);
  return isset($index) ? $status[$index] . ' Days' : $status;
}

function get_import_coll_days($index = null)
{
  $status = array('', '5 Days', '15 Days', '30 Days', '45 Days', '60 Days');
  return isset($index) ? $status[$index] : $status;
}

function get_milestone_import_date($date)
{
  //12/31/2016
  list ($d, $m, $y) = explode("/", $date);
  return $y . "-" . sprintf("%02d", $m) . "-" . sprintf("%02d", $d);
}

function get_milestone_upload_date($date)
{
  //12/31/2016
  list ($d, $m, $y) = explode("-", $date);
  return $y . "-" . sprintf("%02d", $m) . "-" . sprintf("%02d", $d);
}

function csv_from_result($results, $titles, $delim = ',', $newline = "\n", $enclosure = '"')
{
  $out = '';
  // First generate the headings from the table column names
  foreach ($titles as $title) {
    $out .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $title) . $enclosure . $delim;
  }

  $out = substr($out, 0, -strlen($delim)) . $newline;

  foreach ($results as $result) {
    $line = array();
    foreach ($result as $item) {
      $line[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $item) . $enclosure;
    }
    $out .= implode($delim, $line) . $newline;
  }
  return $out;
}

/* Filter session handling */
function filter_save($session_name, $data)
{
  $CI = &get_instance();
  $CI->session->set_userdata($session_name, $data);
}

function filter_remove($session_name)
{
  $CI = &get_instance();
  $CI->session->unset_userdata($session_name);
}


function get_filter_data($session_name)
{
  $CI = &get_instance();
  return $CI->session->userdata($session_name) ? $CI->session->userdata($session_name) : '';
}

function filter_value($filter_data, $ind)
{
  return !empty($filter_data) && !empty($filter_data[$ind]) ? $filter_data[$ind] : '';
}

/* Estimation Module */

function str_acronym($str)
{
  $ret = '';
  foreach (explode(' ', $str) as $word) {
    if ($word) {
      $ret .= strtoupper($word[0]);
    }
  }
  return $ret;
}


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
      $options = array('cost'=>'12');
      return password_hash($plainPassword, PASSWORD_BCRYPT, $options);
        // return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Tanımlanamadı';
        }

        return $agent;
    }
}

if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();
                    
        $CI->load->library('email');
        
        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $CI->email->initialize($config);
        
        return $CI;
    }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("Reset Password");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}
function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
}

/**
 * This method used to convert headlines to seo friendly links
 */
if(!function_exists('sef'))
{
    function sef($str, $options = array())
    {
    $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
    $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => true
    );
    $options = array_merge($defaults, $options);
    $char_map = array(
        // Latin
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
        // Greek
        'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
        'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
        'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
        'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
        'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
        'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
        'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
        // Turkish
        'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
        'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
        // Russian
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
        'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
        'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
        'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',
        // Ukrainian
        'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
        'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
        // Czech
        'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
        'Ž' => 'Z',
        'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
        'ž' => 'z',
        // Polish
        'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
        'Ż' => 'Z',
        'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
        'ż' => 'z',
        // Latvian
        'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
        'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
        'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
        'š' => 's', 'ū' => 'u', 'ž' => 'z'
    );
    $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
    $str = trim($str, $options['delimiter']);
    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }   
}

function prezero($val){
  if($val <= 9){
    return '0'.$val;  
  }else{
    return $val;
  }
}

function secintomin($sec){
  if($sec >= 60){
    $splitin=explode('.',($sec/60));
    if(isset($splitin[1])){
      $secs=substr(intval((substr($splitin[1],0,2)*60)/100),0,2);
    }else{
      $secs=0;
    }
    return $splitin[0].':'.prezero($secs);
  }else{
    return '0:'.prezero($sec);  
  }
}

function ordinal($number) {
  $ends = array('th','st','nd','rd','th','th','th','th','th','th');
  if ((($number % 100) >= 11) && (($number%100) <= 13))
    return $number. 'th';
  else
    return $number. $ends[$number % 10];
}

function get_countries($country = NULL){
	$countries = array(""=>"--Select Country--",
		"AF"=>"Afghanistan",
		"AX"=>"Åland Islands",
		"AL"=>"Albania",
		"DZ"=>"Algeria",
		"AS"=>"American Samoa",
		"AD"=>"Andorra",
		"AO"=>"Angola",
		"AI"=>"Anguilla",
		"AQ"=>"Antarctica",
		"AG"=>"Antigua and Barbuda",
		"AR"=>"Argentina",
		"AM"=>"Armenia",
		"AW"=>"Aruba",
		"AU"=>"Australia",
		"AT"=>"Austria",
		"AZ"=>"Azerbaijan",
		"BS"=>"Bahamas",
		"BH"=>"Bahrain",
		"BD"=>"Bangladesh",
		"BB"=>"Barbados",
		"BY"=>"Belarus",
		"BE"=>"Belgium",
		"BZ"=>"Belize",
		"BJ"=>"Benin",
		"BM"=>"Bermuda",
		"BT"=>"Bhutan",
		"BO"=>"Bolivia, Plurinational State of",
		"BA"=>"Bosnia and Herzegovina",
		"BW"=>"Botswana",
		"BV"=>"Bouvet Island",
		"BR"=>"Brazil",
		"IO"=>"British Indian Ocean Territory",
		"BN"=>"Brunei Darussalam",
		"BG"=>"Bulgaria",
		"BF"=>"Burkina Faso",
		"BI"=>"Burundi",
		"KH"=>"Cambodia",
		"CM"=>"Cameroon",
		"CA"=>"Canada",
		"CV"=>"Cape Verde",
		"KY"=>"Cayman Islands",
		"CF"=>"Central African Republic",
		"TD"=>"Chad",
		"CL"=>"Chile",
		"CN"=>"China",
		"CX"=>"Christmas Island",
		"CC"=>"Cocos (Keeling) Islands",
		"CO"=>"Colombia",
		"KM"=>"Comoros",
		"CG"=>"Congo",
		"CD"=>"Congo, The Democratic Republic of the",
		"CK"=>"Cook Islands",
		"CR"=>"Costa Rica",
		"HR"=>"Croatia",
		"CU"=>"Cuba",
		"CY"=>"Cyprus",
		"CZ"=>"Czech Republic",
		"DK"=>"Denmark",
		"DJ"=>"Djibouti",
		"DM"=>"Dominica",
		"DO"=>"Dominican Republic",
		"EC"=>"Ecuador",
		"EG"=>"Egypt",
		"SV"=>"El Salvador",
		"GQ"=>"Equatorial Guinea",
		"ER"=>"Eritrea",
		"EE"=>"Estonia",
		"ET"=>"Ethiopia",
		"FK"=>"Falkland Islands (Malvinas)",
		"FO"=>"Faroe Islands",
		"FJ"=>"Fiji",
		"FI"=>"Finland",
		"FR"=>"France",
		"GF"=>"French Guiana",
		"PF"=>"French Polynesia",
		"TF"=>"French Southern Territories",
		"GA"=>"Gabon",
		"GM"=>"Gambia",
		"GE"=>"Georgia",
		"DE"=>"Germany",
		"GH"=>"Ghana",
		"GI"=>"Gibraltar",
		"GR"=>"Greece",
		"GL"=>"Greenland",
		"GD"=>"Grenada",
		"GP"=>"Guadeloupe",
		"GU"=>"Guam",
		"GT"=>"Guatemala",
		"GG"=>"Guernsey",
		"GN"=>"Guinea",
		"GW"=>"Guinea-Bissau",
		"GY"=>"Guyana",
		"HT"=>"Haiti",
		"HM"=>"Heard Island and McDonald Islands",
		"VA"=>"Holy See (Vatican City State)",
		"HN"=>"Honduras",
		"HK"=>"Hong Kong",
		"HU"=>"Hungary",
		"IS"=>"Iceland",
		"IN"=>"India",
		"ID"=>"Indonesia",
		"IR"=>"Iran, Islamic Republic of",
		"IQ"=>"Iraq",
		"IE"=>"Ireland",
		"IM"=>"Isle of Man",
		"IL"=>"Israel",
		"IT"=>"Italy",
		"JM"=>"Jamaica",
		"JP"=>"Japan",
		"JE"=>"Jersey",
		"JO"=>"Jordan",
		"KZ"=>"Kazakhstan",
		"KE"=>"Kenya",
		"KI"=>"Kiribati",
		"KR"=>"Korea, Republic of",
		"KW"=>"Kuwait",
		"KG"=>"Kyrgyzstan",
		"LV"=>"Latvia",
		"LB"=>"Lebanon",
		"LS"=>"Lesotho",
		"LR"=>"Liberia",
		"LY"=>"Libyan Arab Jamahiriya",
		"LI"=>"Liechtenstein",
		"LT"=>"Lithuania",
		"LU"=>"Luxembourg",
		"MO"=>"Macao",
		"MK"=>"Macedonia, The Former Yugoslav Republic of",
		"MG"=>"Madagascar",
		"MW"=>"Malawi",
		"MY"=>"Malaysia",
		"MV"=>"Maldives",
		"ML"=>"Mali",
		"MT"=>"Malta",
		"MH"=>"Marshall Islands",
		"MQ"=>"Martinique",
		"MR"=>"Mauritania",
		"MU"=>"Mauritius",
		"YT"=>"Mayotte",
		"MX"=>"Mexico",
		"FM"=>"Micronesia, Federated States of",
		"MD"=>"Moldova, Republic of",
		"MC"=>"Monaco",
		"MN"=>"Mongolia",
		"ME"=>"Montenegro",
		"MS"=>"Montserrat",
		"MA"=>"Morocco",
		"MZ"=>"Mozambique",
		"MM"=>"Myanmar",
		"NA"=>"Namibia",
		"NR"=>"Nauru",
		"NP"=>"Nepal",
		"NL"=>"Netherlands",
		"AN"=>"Netherlands Antilles",
		"NC"=>"New Caledonia",
		"NZ"=>"New Zealand",
		"NI"=>"Nicaragua",
		"NE"=>"Niger",
		"NG"=>"Nigeria",
		"NU"=>"Niue",
		"NF"=>"Norfolk Island",
		"MP"=>"Northern Mariana Islands",
		"NO"=>"Norway",
		"OM"=>"Oman",
		"PK"=>"Pakistan",
		"PW"=>"Palau",
		"PS"=>"Palestinian Territory, Occupied",
		"PA"=>"Panama",
		"PG"=>"Papua New Guinea",
		"PY"=>"Paraguay",
		"PE"=>"Peru",
		"PH"=>"Philippines",
		"PN"=>"Pitcairn",
		"PL"=>"Poland",
		"PT"=>"Portugal",
		"PR"=>"Puerto Rico",
		"QA"=>"Qatar",
		"RE"=>"Réunion",
		"RO"=>"Romania",
		"RU"=>"Russian Federation",
		"RW"=>"Rwanda",
		"BL"=>"Saint Barthélemy",
		"SH"=>"Saint Helena, Ascension and Tristan Da Cunha",
		"KN"=>"Saint Kitts and Nevis",
		"LC"=>"Saint Lucia",
		"MF"=>"Saint Martin",
		"PM"=>"Saint Pierre and Miquelon",
		"VC"=>"Saint Vincent and the Grenadines",
		"WS"=>"Samoa",
		"SM"=>"San Marino",
		"ST"=>"Sao Tome and Principe",
		"SA"=>"Saudi Arabia",
		"SN"=>"Senegal",
		"RS"=>"Serbia",
		"SC"=>"Seychelles",
		"SL"=>"Sierra Leone",
		"SG"=>"Singapore",
		"SK"=>"Slovakia",
		"SI"=>"Slovenia",
		"SB"=>"Solomon Islands",
		"SO"=>"Somalia",
		"ZA"=>"South Africa",
		"GS"=>"South Georgia and the South Sandwich Islands",
		"ES"=>"Spain",
		"LK"=>"Sri Lanka",
		"SD"=>"Sudan",
		"SR"=>"Suriname",
		"SJ"=>"Svalbard and Jan Mayen",
		"SZ"=>"Swaziland",
		"SE"=>"Sweden",
		"CH"=>"Switzerland",
		"SY"=>"Syrian Arab Republic",
		"TW"=>"Taiwan, Province of China",
		"TJ"=>"Tajikistan",
		"TZ"=>"Tanzania, United Republic of",
		"TH"=>"Thailand",
		"TL"=>"Timor-Leste",
		"TG"=>"Togo",
		"TK"=>"Tokelau",
		"TO"=>"Tonga",
		"TT"=>"Trinidad and Tobago",
		"TN"=>"Tunisia",
		"TR"=>"Turkey",
		"TM"=>"Turkmenistan",
		"TC"=>"Turks and Caicos Islands",
		"TV"=>"Tuvalu",
		"UG"=>"Uganda",
		"UA"=>"Ukraine",
		"AE"=>"United Arab Emirates",
		"GB"=>"United Kingdom",
		"US"=>"United States",
		"UM"=>"United States Minor Outlying Islands",
		"UY"=>"Uruguay",
		"UZ"=>"Uzbekistan",
		"VU"=>"Vanuatu",
		"VE"=>"Venezuela, Bolivarian Republic of",
		"VN"=>"Viet Nam",
		"VG"=>"Virgin Islands, British",
		"VI"=>"Virgin Islands, U.S.",
		"WF"=>"Wallis and Futuna",
		"EH"=>"Western Sahara",
		"YE"=>"Yemen",
		"ZM"=>"Zambia",
		"ZW"=>"Zimbabwe");
	return isset($country) ? $countries[$country] : $countries;
}

function get_ticket_status($status = NULL){
	$det = array('0'=>'Open', '1'=>'Closed');
	return isset($status) ? $det[$status] : $det;
}

function get_status($status = NULL){
	$det = array('0'=>'Un-Published', '1'=>'Published');
	return isset($status) ? $det[$status] : $det;
}

function otp_generator($len = 6){
	$characters = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$string = '';
	$max = strlen($characters) - 1;
	for ($i = 0; $i < $len; $i++) {
	    $string .= $characters[mt_rand(0, $max)];
	}

	return $string;
}