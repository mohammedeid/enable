<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_m extends MY_Model{

    function get_tickets(){
        $query = $this->db->query("SELECT a.id,b.name,a.custom_id,a.subject,a.created_on,a.status FROM ticket AS a LEFT OUTER JOIN user AS b ON b.id=a.user_id "); //where a.status=0
        if ($query->num_rows() > 0) {
            return $query->result();
        }else {
            return false;
        }
    }

    function view_ticket_response($ticketID){
		$query = $this->db->query("SELECT a.id AS ticketID,
								a.custom_id,a.subject,a.description,a.created_on AS ticket_date,
                                b.msg,b.created_on AS reply_date,c.name AS ticket_name,
								e.name AS reply_name,e.image_name AS image_name
								FROM ticket AS a 
								LEFT OUTER JOIN ticket_reply AS b ON b.ticket_id=a.id 
								LEFT OUTER JOIN user AS c ON c.id=a.user_id 
								LEFT OUTER JOIN user AS e ON e.id=b.user_id 
								WHERE a.id=$ticketID ORDER BY b.id DESC");
    	if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

    function saveTicketResponse($data){
        $this->db->insert('ticket_reply',$data);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_user_tickets($user_id){
        $query = $this->db->query("SELECT a.id,b.name,a.custom_id,a.subject,a.created_on,a.status FROM ticket AS a LEFT OUTER JOIN user AS b ON b.id=a.user_id where a.user_id=$user_id"); 
        if ($query->num_rows() > 0) {
            return $query->result();
        }else {
            return false;
        }
    }

    function getCount($tb_name){
		return $this->db->count_all($tb_name);
	}
    

}
?>