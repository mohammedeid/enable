<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends MY_Model{

    function get_user(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('deleted !=', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_user_detail($id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }

    function get_staff(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('deleted !=', 1);
        $this->db->where('role_id', 2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_team(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('deleted !=', 1);
        $this->db->where('role_id', 3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_staff_user($staff_id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('deleted !=', 1);
        $this->db->where('staff_id', $staff_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_team_user($team_id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('deleted !=', 1);
        $this->db->where('team_id', $team_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }


}
?>