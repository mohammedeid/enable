<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_m extends MY_Model{

    function get_course(){
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('deleted', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_course_detail($id){
		$this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
	}

}
?>