<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Settings_m extends MY_Model{

    function updateSettings($id,$det){
        $this->db->where('id', $id);
        $this->db->update('settings', $det);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
    
}

?>