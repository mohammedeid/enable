<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Voucher_m extends MY_Model{

    function get_voucher(){
        $this->db->select('*');
        $this->db->from('voucher');
        $this->db->where('deleted', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_voucher_detail($id){
		$this->db->select('*');
        $this->db->from('voucher');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
	}

}
?>