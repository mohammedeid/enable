<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends MY_Model{

     /**
     * This function is used to get last login info by user id
     * @param number $userId : This is user id
     * @return number $result : This is query result
     */
    function lastLoginInfo($userId){
        $this->db->select('BaseTbl.createdDtm');
        $this->db->where('BaseTbl.userId', $userId);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('log as BaseTbl');
        return $query->row();
    }

    function custom_login($moodle_id){
        $this->db->select('u.*, r.id as role_id, r.name as role_name');
        $this->db->from('user as u');
        $this->db->join('roles as r','r.id = u.role_id');
        $this->db->where('u.moodle_id', $moodle_id);
        $this->db->where('u.deleted', 0);
        $query = $this->db->get();

        $user = $query->row();
        
        if(!empty($user)){
           return $user;
        } else {
            return array();
        }
    }


    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('id');
        $this->db->from('admin');
        $this->db->where('email_id', $email);
        $this->db->where('deleted', 0);
        $query = $this->db->get();
        $user = $query->row();

        if(!empty($user)){
            return $user;
            // if(verifyHashedPassword($password, $user[0]->password)){
            //     return $user;
            // } else {
            //     return array();
            // }
        } else {
            return array();
        }
    }

   
}