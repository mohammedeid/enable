<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Forum_m extends MY_Model{

    function get_forum(){
        $this->db->select('*');
        $this->db->from('forum');
        $this->db->where('deleted', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    function get_forum_detail($id){
		$this->db->select('*');
        $this->db->from('forum');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
	}

    function saveForumResponse($data){
        $this->db->insert('forum_reply',$data);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_forum_response($forumID){
		$query = $this->db->query("SELECT b.name AS reply_name, b.image_name AS image_name,
								a.message,a.created_on AS reply_date,a.tot_likes,a.id
								FROM forum_reply AS a 
								LEFT OUTER JOIN user AS b ON b.id=a.user_id 
								WHERE a.forum_id=$forumID AND a.deleted=0 ORDER BY a.id DESC");
    	if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

    function deleteForumResponse($replyID){
        $this->db->where('id', $replyID);
        $this->db->update('forum_reply', array('deleted' => 1));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function check_response_like($replyID,$userID){
        $query = $this->db->query("SELECT id FROM forum_like WHERE forum_reply_id=$replyID AND user_id=$userID ");
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
	}

    function deleteResponseLike($replyID,$userID){
        $this->db->where('forum_reply_id', $replyID);
        $this->db->where('user_id', $userID);
        $this->db->delete('forum_like');
        if ($this->db->affected_rows()) {
            $this->db->query("UPDATE forum_reply SET tot_likes=tot_likes-1 WHERE id=$replyID");
            return true;
        } else {
            return false;
        }
    }

    function addResponseLike($replyID,$data){
        $this->db->insert('forum_like',$data);
        if ($this->db->affected_rows()) {
            $this->db->query("UPDATE forum_reply SET tot_likes=tot_likes+1 WHERE id=$replyID");
            return true;
        } else {
            return false;
        }
    }

}
?>