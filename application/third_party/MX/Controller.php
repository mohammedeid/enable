<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller 
{
	// User session variables
	protected $role = '';
	protected $vendorId = '';
	protected $name = '';
	protected $roleText = '';
	protected $global = array ();
	protected $lastLogin = '';
	protected $status = '';
	public $autoload = array();

	
	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);
	}
	
	public function __get($class) 
	{
		return CI::$APP->$class;
	}


	public function response($data = NULL) {
		$this->output->set_status_header ( 200 )->set_content_type ( 'application/json', 'utf-8' )->set_output ( json_encode ( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) )->_display ();
		exit ();
	}
	
	/**
	 * This function used to check the user is logged in or not
	 */
	function isLoggedIn() {

		$isLoggedIn = $this->session->userdata('enable_lms_userdata' )[ 'isLoggedIn' ];
		
		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'login' );
		} else {
			$this->datas();
		}
	}
	



	/**
	 * This function is used to view no access view
	 */
	public function accesslogincontrol()
    {
            redirect(noaccess);
    }
	
	/**
	 * This function is used to logged out user from system
	 */
	function logout() {

		$this->session->sess_destroy();
		
		redirect ( 'login' );
	}

	
	/**
	 * This function used to load user sessions
	 */
	function datas()
	{
		$this->role = $this->session->userdata ('enable_lms_userdata' ) ['role'];
		$this->vendorId = $this->session->userdata ('enable_lms_userdata' ) ['userId'];
		$this->name = $this->session->userdata ('enable_lms_userdata' ) ['frist_name'] ." ". $this->session->userdata ('enable_lms_userdata' ) ['last_name'];
		$this->roleText = $this->session->userdata('enable_lms_userdata' ) ['roleText'];
		$this->lastLogin = $this->session->userdata ('enable_lms_userdata' )[ 'lastLogin' ];
		$this->status = $this->session->userdata ('enable_lms_userdata' ) [ 'status' ];
		
		
		$this->global ['name'] = $this->name;
		$this->global ['role'] = $this->role;
		$this->global ['role_text'] = $this->roleText;
		$this->global ['last_login'] = $this->lastLogin;
		$this->global ['status'] = $this->status;
		
	}
	
	/**
	 * This function insert into log to the log table
	 */
	function logrecord($process,$processFunction){
		$this->datas();
		$logInfo = array("userId"=>$this->vendorId,
		"userName"=>$this->name,
		"process"=>$process,
		"processFunction"=>$processFunction,
		"userRoleId"=>$this->role,
		"userRoleText"=>$this->roleText,
		"userIp"=>$_SERVER['REMOTE_ADDR'],
		"userAgent"=>getBrowserAgent(),
		"agentString"=>$this->agent->agent_string(),
		"platform"=>$this->agent->platform()
		);	
		$this->load->model('login_model');
		$this->login_model->loginsert($logInfo);
	}
}